<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_kelas extends CI_model {

	
	public function getdata($key)
	{
		$this->db->where('id_kelas',$key);
		$hasil = $this->db->get('kelas');
		return $hasil;
	}

	public function getupdate($key,$data)
	{
		$this->db->where('id_kelas',$key);
		$this->db->update('kelas',$data);
	}

	public function getinsert($data)
	{
	$this->db->insert('kelas',$data);
	}

	public function getdelete($key)
	{
		$this->db->where('id_kelas',$key);
		$this->db->delete('kelas');
	}
	public function getlistkelas()
	{
		return $this->db->get('kelas');
	}

	public function tampiltahunajaran($key)
	{
	 	$this->db->where('id_tahun_ajaran',$key);
	 	$query = $this->db->get('tahun_ajaran');
	 	if($query->num_rows())
	 	{
	 		foreach ($$query->result() as $row) 
	 		{
	 			$hasil = $row->tahun_ajaran;
	 		}

	 	}
	 	else
	 	{
	 		$hasil ="";
	 	}
	 	return $hasil;
	}
}
