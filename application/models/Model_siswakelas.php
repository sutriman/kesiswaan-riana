<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_siswakelas extends CI_model {

	
	public function get_data($thn='',$kelas='')
	{
		#$this->db->order_by('s.NISN','DESC');
		#$this->db->join('siswa as sw','s.NISN = sw.NISN');
		#$this->db->join('tahun_ajaran as t','s.id_tahun_ajaran = t.id_tahun_ajaran');
		#$this->db->join('kelas as k','s.id_kelas = k.id_kelas');
		#return $this->db->get('siswa_kelas as s');
		$sql = "SELECT
			s.nisn,
			s.nama_siswa,
			IF(s.j_kelamin='L','Laki-laki','Perempuan') AS jenis_kelamin,
			s.tmpt_lhr,
			s.`tgl_lhr`,
			k.`nama_kelas`
			FROM siswa_kelas AS sk
			JOIN siswa AS s ON sk.`NISN` = s.`NISN`
			JOIN kelas k ON k.`id_kelas` = sk.`id_kelas`
			WHERE sk.`id_tahun_ajaran` = ? AND sk.id_kelas = ?
			ORDER BY s.NISN
		";

		$result = $this->db->query($sql,array($thn,$kelas));
		return $result->result();
	}

	public function getupdate($key,$data)
	{
		$this->db->where('NISN',$key);
		$this->db->update('siswa_kelas',$data);
	}

	public function getinsert2($data2)
	{
	$this->db->insert('siswa_kelas',$data2);
	}

	public function getdelete($key)
	{
		$this->db->where('NISN',$key);
		$this->db->delete('siswa_kelas');
	}
	
	public function getlistkelas()
	{
		return $this->db->get('kelas');
	}

	public function ambil_kelas()
	{
		return $this->db->get('siswa_kelas');
	}
}