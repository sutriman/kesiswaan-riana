<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_siswa extends CI_model {

	public function getdata()
	{
		// $this->db->where('NISN',$key);
		$hasil = $this->db->get('siswa');
		return $hasil;
	}

	public function get_data()
	{
		$this->db->order_by('s.NISN','DESC');
		$this->db->join('siswa as sw','s.NISN = sw.NISN');
		$this->db->join('tahun_ajaran as t','s.id_tahun_ajaran = t.id_tahun_ajaran');
		$this->db->join('kelas as k','s.id_kelas = k.id_kelas');
		$this->db->join('agama', 'agama.id_agama = sw.id_agama');
		// $this->db->join('kecamatan as kec', 's.id_kecamatan = kec.id_kecamatan');
		$this->db->group_by('s.NISN');
		return $this->db->get('siswa_kelas as s');
	}


	public function getupdate($key,$data)
	{
		$this->db->where('NISN',$key);
		$this->db->update('siswa',$data);
	}

	public function getupdate2($key,$data)
	{
		$this->db->where('NISN',$key);
		$this->db->update('siswa_kelas',$data);
	}

	public function getinsert($data)
	{
	$this->db->insert('siswa',$data);
	}

	public function getinsert2($data2)
	{
	$this->db->insert('siswa_kelas',$data2);
	}

	public function getdelete($key)
	{
		$this->db->where('NISN',$key);
		$this->db->delete('siswa');
	}
	
	public function getlistkelas()
	{
		return $this->db->get('kelas');
	}
}