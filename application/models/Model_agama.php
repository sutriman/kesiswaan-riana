<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_agama extends CI_model {

	
	public function getdata($key)
	{
		$this->db->where('id_agama',$key);
		$hasil = $this->db->get('agama');
		return $hasil;
	}

	public function getupdate($key,$data)
	{
		$this->db->where('id_agama',$key);
		$this->db->update('agama',$data);
	}

	public function getinsert($data)
	{
	$this->db->insert('agama',$data);
	}

	public function getdelete($key)
	{
		$this->db->where('id_agama',$key);
		$this->db->delete('agama');
	}
	
}
