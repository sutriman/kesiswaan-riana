<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_laporan extends CI_model {



	public function tahun_lahir() {
		$a = $this->db->query('SELECT SUBSTRING(tgl_lhr,1,4) AS thn_lhr FROM siswa GROUP BY SUBSTRING(tgl_lhr,1,4)')->result();
		return $a;
	}

	public function tahun_lahir2() {
		$a = $this->db->query('SELECT SUBSTRING(tgl_lhr,1,9) AS thn_lhr FROM siswa GROUP BY SUBSTRING(tgl_lhr,1,9)')->result();
		return $a;
	}
	

	public function hitung_siswa($tahun, $idkelas, $idtahunajaran) {
		// $this->db->select('COUNT(*) as jumlah');
		// $this->db->from('siswa_kelas');
		// $this->db->join('kelas', 'kelas.id_kelas = siswa_kelas.id_kelas');
		// $this->db->join('siswa', 'siswa.NISN = siswa_kelas.NISN');
		// if ($idtahunajaran != '') {
		// 	$this->db->join('tahun_ajaran', 'tahun_ajaran.id_tahun_ajaran = siswa_kelas.id_tahun_ajaran');
		// 	$this->db->where('tahun_ajaran.id_tahun_ajaran', $idtahunajaran);
		// }

		// if ($tahun != '') {
		// 	$this->db->join('tahun_ajaran', 'tahun_ajaran.id_tahun_ajaran = siswa_kelas.id_tahun_ajaran');
		// 	$this->db->where('tahun_ajaran.id_tahun_ajaran', $idtahunajaran);
		// }
		// $this->db->like('siswa.tgl_lhr', $tahun);
		// $this->db->where('kelas.id_kelas', $idkelas);
		// return $this->db->get()->result();

		$q = $this->db->query("SELECT 
					  COUNT(siswa.`NISN`) AS jumlah 
					FROM
					  siswa 
					  JOIN siswa_kelas 
					    ON siswa_kelas.`NISN` = siswa.`NISN` 
					  JOIN tahun_ajaran 
					    ON tahun_ajaran.`id_tahun_ajaran` = siswa_kelas.`id_tahun_ajaran` 
					WHERE YEAR(siswa.`tgl_lhr`) = ".$tahun." 
					  AND siswa_kelas.`id_kelas` = ".$idkelas." 
					  AND tahun_ajaran.`id_tahun_ajaran` = 1995 ");
		return $q->result();
	}

	// public function hitung_siswa2($tahun, $idkelas, $idtahunajaran) {
	// 	$this->db->select('COUNT(*) as jumlah');
	// 	$this->db->from('siswa_kelas');
	// 	$this->db->join('kelas', 'kelas.id_kelas = siswa_kelas.id_kelas');
	// 	$this->db->join('siswa', 'siswa.NISN = siswa_kelas.NISN');
	// 	if ($idtahunajaran != '') {
	// 		$this->db->join('tahun_ajaran', 'tahun_ajaran.id_tahun_ajaran = siswa_kelas.id_tahun_ajaran');
	// 		$this->db->where('tahun_ajaran.id_tahun_ajaran', $idtahunajaran);
	// 	}

	// 	if ($tgl_lhr != '') {
	// 		$this->db->join('tahun_ajaran', 'tahun_ajaran.id_tahun_ajaran = siswa_kelas.id_tahun_ajaran');
	// 		$this->db->where('tahun_ajaran.id_tahun_ajaran', $idtahunajaran);
	// 	}
	// 	$this->db->like('siswa.tgl_lhr', $tahun);
	// 	$this->db->where('kelas.id_kelas', $idkelas);
	// 	return $this->db->get()->result();
	// }

	public function laporan1($tahunajar) {


			if ($tahunajar != '') {
				$siswa = $this->db->query("SELECT k.id_kelas, k.nama_kelas, SUM(IF(s.j_kelamin = 'L',1,0)) AS jumlahlaki, SUM(IF(s.j_kelamin = 'P',1,0)) AS jumlahperempuan , SUM(IF(s.j_kelamin = 'L',1,0)) + SUM(IF(s.j_kelamin = 'P',1,0)) AS jumlahsiswa FROM kelas AS k
			JOIN siswa_kelas AS sk ON k.id_kelas = sk.id_kelas
			JOIN siswa AS s ON s.NISN = sk.NISN
			JOIN tahun_ajaran AS ta ON ta.id_tahun_ajaran = sk.id_tahun_ajaran
			WHERE sk.id_tahun_ajaran = $tahunajar
			GROUP BY id_kelas;");

		} else {
			$siswa = $this->db->query("SELECT k.id_kelas, k.nama_kelas, SUM(IF(s.j_kelamin = 'L',1,0)) AS jumlahlaki, SUM(IF(s.j_kelamin = 'P',1,0)) AS jumlahperempuan , SUM(IF(s.j_kelamin = 'L',1,0)) + SUM(IF(s.j_kelamin = 'P',1,0)) AS jumlahsiswa  FROM kelas AS k
			LEFT JOIN siswa_kelas AS sk ON k.id_kelas = sk.id_kelas
			LEFT JOIN siswa AS s ON s.NISN = sk.NISN
			GROUP BY id_kelas;");
		}

			return $siswa;
			}

	public function laporan2($tahunajar2) {

		if ($tahunajar2 != '') {
		
		$siswa = $this->db->query("select k.id_kelas, k.nama_kelas, SUM(IF(s.id_kabupaten ='105',1,0)) AS dalamkota, SUM(IF(s.id_kabupaten='101,102,103,104',1,0)) AS luarkota FROM kelas AS k
			JOIN siswa_kelas AS sk ON k.id_kelas = sk.id_kelas
			JOIN siswa AS s ON s.NISN = sk.NISN
			JOIN tahun_ajaran AS ta ON ta.id_tahun_ajaran = sk.id_tahun_ajaran
			WHERE sk.id_tahun_ajaran = $tahunajar2
			GROUP BY id_kelas;");
	} else {
		$siswa = $this->db->query("select k.id_kelas, k.nama_kelas, SUM(IF(s.id_kabupaten ='105',1,0)) AS dalamkota, SUM(IF(s.id_kabupaten='101,102,103,104',1,0)) AS luarkota FROM kelas AS k
			LEFT JOIN siswa_kelas AS sk ON k.id_kelas = sk.id_kelas
			LEFT JOIN siswa AS s ON s.NISN = sk.NISN
			GROUP BY id_kelas;");
	}
		return $siswa;
	}
	
	
	public function laporan3($tahunajar3) {

		if ($tahunajar3 != '') {

		$siswa = $this->db->query("SELECT k.id_kelas, k.nama_kelas, SUM(IF(s.id_agama='1',1,0)) 'islam', 
		SUM(IF(s.id_agama='2',1,0)) 'kristen', 
		SUM(IF(s.id_agama='3',1,0)) 'katholik', 
		SUM(IF(s.id_agama='4',1,0)) 'hindu', 
		SUM(IF(a.id_agama='5',1,0)) 'budha', 
		SUM(IF(a.id_agama='6',1,0)) 'lainnya'
		 FROM kelas AS k
		LEFT JOIN siswa_kelas AS sk ON k.id_kelas = sk.id_kelas
		LEFT JOIN siswa AS s ON s.NISN = sk.NISN
		LEFT JOIN agama AS a ON a.id_agama = s.NISN
		JOIN tahun_ajaran AS ta ON ta.id_tahun_ajaran = sk.id_tahun_ajaran
			WHERE sk.id_tahun_ajaran = $tahunajar3
		GROUP BY id_kelas;");

	}  else {
		$siswa = $this->db->query("SELECT k.id_kelas, k.nama_kelas, SUM(IF(s.id_agama='1',1,0)) 'islam', 
		SUM(IF(s.id_agama='2',1,0)) 'kristen', 
		SUM(IF(s.id_agama='3',1,0)) 'katholik', 
		SUM(IF(s.id_agama='4',1,0)) 'hindu', 
		SUM(IF(a.id_agama='5',1,0)) 'budha', 
		SUM(IF(a.id_agama='6',1,0)) 'lainnya'
		 FROM kelas AS k
		LEFT JOIN siswa_kelas AS sk ON k.id_kelas = sk.id_kelas
		LEFT JOIN siswa AS s ON s.NISN = sk.NISN
		LEFT JOIN agama AS a ON a.id_agama = s.NISN
		GROUP BY id_kelas;");

	}

		return $siswa;
	}
	
	public function laporan4($tahunajar4) {


		$siswa = $this->db->query("SELECT k.id_kelas, k.nama_kelas,
		SUBSTRING(tgl_lhr,1,4) AS thn_lhr,
		COUNT(SUBSTRING(tgl_lhr,1,4)) AS jumlah
		FROM kelas AS k
		LEFT JOIN siswa_kelas AS sk ON k.id_kelas = sk.id_kelas
		LEFT JOIN siswa AS s ON s.NISN = sk.NISN
		GROUP BY k.id_kelas
		ORDER BY k.`id_kelas`;");

		return $siswa;
	}	



	public function laporan5($tahunajar5) {


	$siswa = $this->db->query("SELECT k.id_kelas, k.nama_kelas,
		SUBSTRING(tgl_lhr,1,9) AS thn_lhr,
		COUNT(SUBSTRING(tgl_lhr,1,9)) AS jumlah
		FROM kelas AS k
		LEFT JOIN siswa_kelas AS sk ON k.id_kelas = sk.id_kelas
		LEFT JOIN siswa AS s ON s.NISN = sk.NISN
		GROUP BY k.id_kelas
		ORDER BY k.`id_kelas`;");

		return $siswa;
}
}