<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_tahun_ajaran extends CI_model {

	
	public function getdata($key)
	{
		$this->db->where('id_tahun_ajaran',$key);
		$hasil = $this->db->get('tahun_ajaran');
		return $hasil;
	}

	public function getupdate($key,$data)
	{
		$this->db->where('id_tahun_ajaran',$key);
		$this->db->update('tahun_ajaran',$data);
	}

	public function getinsert($data)
	{
	$this->db->insert('tahun_ajaran',$data);
	}

	public function getdelete($key)
	{
		$this->db->where('id_tahun_ajaran',$key);
		$this->db->delete('tahun_ajaran');
	}
	
}
