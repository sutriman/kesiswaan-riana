<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TahunAjaran extends CI_Controller {

	
	public function index()
	{
		$this->model_security->getsecurity();
		$this->load->model('Model_tahun_ajaran');
		$isi['content']			= 'tahunajaran/tampil_tahunajaran';
		$isi['judul']			='Data';
		$isi['sub_judul']		= 'Tahun Ajaran';
		$isi['id_tahun_ajaran']		= "";
		$isi['tahun_ajaran']		= "";
		$isi['status']				= "";
		$isi['data']			= $this->db->get('tahun_ajaran');
		$this->load->view('tampilan_home',$isi);
	}
	
		public function tambah()
	{
		$this->model_security->getsecurity();
		$this->load->model('Model_tahun_ajaran');
		$isi['content']				= 'tahunajaran/tampil_tahunajaran';
		$isi['judul']				='Data';
		$isi['sub_judul']			= 'Tahun Ajaran';
		$isi['id_tahun_ajaran']		= "";
		$isi['tahun_ajaran']		= "";
		$isi['status']				= "";
		$this->load->view('tampilan_home',$isi);
	}

	public function edit()
	{
		$this->model_security->getsecurity();
		$this->load->model('Model_tahun_ajaran');
		$isi['content']			= 'tahunajaran/tampil_tahunajaran';
		$isi['judul']			='Data';
		$isi['sub_judul']		= 'Edit Tahun Ajaran';
		$isi['data']			= $this->db->get('tahun_ajaran');
		$key = $this->uri->segment(3);
		$this->db->where('id_tahun_ajaran',$key);
		$query = $this->db->get('tahun_ajaran');
		if($query->num_rows()>0)
		{
			foreach ($query->result() as $row)

			{
					$isi['id_tahun_ajaran']			= $row->id_tahun_ajaran;
					$isi['tahun_ajaran']			= $row->tahun_ajaran;
					$isi['status']					= $row->status;
			}
		}
		else
		{
					$isi['id_tahun_ajaran']			= "";
					$isi['tahun_ajaran']			= "";
					$isi['status']					= "";
					
		}

		$this->load->view('tampilan_home',$isi);

	}

		public function simpan()
	{
		$this->model_security->getsecurity();
		$key = $this->input->post('id_tahun_ajaran');
		$data['id_tahun_ajaran']		=		$this->input->post('id_tahun_ajaran');
		$data['tahun_ajaran']			=		$this->input->post('tahun_ajaran');
		$data['status']					=		$this->input->post('status');
		
		$this->load->model('Model_tahun_ajaran');
		$query = $this->Model_tahun_ajaran->getdata($key);
				if($query->num_rows()>0)
		{


			$this->Model_tahun_ajaran->getupdate($key,$data);
			$this->session->set_flashdata('info','Data sukses di update');
		}
		else
		{
		$this->Model_tahun_ajaran->getinsert($data);
		$this->session->set_flashdata('info','Data sukses di simpan');
	}
		
	redirect('tahunajaran');

	}

public function delete($id)
{
$this->model_security->getsecurity();
	$this->db->where('id_tahun_ajaran', $id);
	$this->db->delete('siswa_kelas');

	$this->db->where('id_tahun_ajaran', $id);
	$this->db->delete('tahun_ajaran');
		redirect('tahunajaran');
}
}