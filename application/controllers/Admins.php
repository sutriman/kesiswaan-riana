<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admins extends CI_Controller {

	
	public function index()
	{
		$this->model_security->getsecurity();
		$isi['content']			= 'admins/tampilan_admin';
		$isi['judul']			='Data';
		$isi['sub_judul']		= 'admin';
		$isi['id_username']		= "";
		$isi['username']		= "";	
		$isi['password']		= "";	
		$isi['data']			= $this->db->get('admins');
		$this->load->view('tampilan_home',$isi);
	}
	
		public function tambah()
	{
		$this->model_security->getsecurity();
		$isi['content']			= 'admins/tampilan_admin';
		$isi['judul']			='Data';
		$isi['sub_judul']		= 'admin';
		$isi['id_username']		= "";
		$isi['username']		= "";
		$isi['password']		= "";
		$this->load->view('tampilan_home',$isi);
	}

	public function edit()
	{
		$this->model_security->getsecurity();
		$isi['content']			= 'admins/tampilan_admin';
		$isi['judul']			='Data';
		$isi['sub_judul']		= 'Reset Password Admin';
		
if(isset($_POST["simpan"])) {
    // $password_old   = no_injex($_POST['password_old']); //Password lama $this->input->post('id_username');
    // $password_new   = no_injex($_POST['password_new']); //Password baru
    // $password_conf  = no_injex($_POST['password_conf']); //Konfirmasi password

    $key = $this->input->post('password');
		$data['password_old']			=		$this->input->post('password_old');
		$data['password_new']			=		$this->input->post('password_new');
		$data['password_conf'] 			= 		md5($this->input->post('password_conf'));

    if (empty(trim($password_old)) || empty(trim($password_new)) || empty(trim($password_conf)) ) {
            echo "<script>alert('Form tidak boleh ada yang kosong!');</script>";
    } else {

        $sql  = mysql_query("SELECT * FROM tbl_admin WHERE id = '$_SESSION[uid]' ");
        $data = mysql_fetch_array($sql);

        $pass = password_verify($password_old, $data['password']);

        //die(var_dump($pass));
        
        if ($pass === TRUE) {

            $pass_new  = password_hash($password_new, PASSWORD_DEFAULT, ['cost'=>12]);
            //$pass_conf = password_hash($password_conf, PASSWORD_DEFAULT, ['cost'=>12]);
            $conf = password_verify($password_conf, $pass_new);

            //die(var_dump($conf));

            if($conf === FALSE) {
                echo "<script>alert('Gagal mengganti password! Password tidak sama!');</script>";
            } else {
                $q = u_query(
                "admins",
                "password = '$pass_new'",
                "id = $_SESSION[uid]"
                );
                echo msg("success","Data berhasil diedit.");
            }

        } else {
            echo "<script>alert('Gagal mengganti password! Password tidak terdaftar!');window.location='index.php?page=pengaturan';</script>";
        }
    }

}
					
		$this->load->view('tampilan_home',$isi);

	}

		public function simpan()
	{
		$this->model_security->getsecurity();
		$key = $this->input->post('id_username');
		$data['id_username']			=		$this->input->post('id_username');
		$data['username']				=		$this->input->post('username');
		$data['password'] 				= 		md5($this->input->post('password'));
		$this->load->model('model_admin');
		$query = $this->model_admin->getdata($key);
		if($query->num_rows()>0)
		{
			$this->model_admin->getupdate($key,$data);
			$this->session->set_flashdata('info','Data sukses di update');
		}
		else
		{
		$this->model_admin->getinsert($data);
		$this->session->set_flashdata('info','Data sukses di simpan');
	}
	redirect('admin');

}
public function delete($id)
{
$this->model_security->getsecurity();
	$this->db->where('id_username', $id);
	$this->db->delete('admin');

	$this->db->where('id_username', $id);
	$this->db->delete('siswa');
		redirect('admin');
}
}