<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class agama extends CI_Controller {

	
	public function index()
	{
		$this->model_security->getsecurity();
		$isi['content']			= 'agama/tampilan_agama';
		$isi['judul']			='Data';
		$isi['sub_judul']		= 'Agama';
		$isi['id_agama']		= "";
		$isi['nama_agama']		= "";		
		$isi['data']			= $this->db->get('agama');
		$this->load->view('tampilan_home',$isi);
	}
	
		public function tambah()
	{
		$this->model_security->getsecurity();
		$isi['content']			= 'agama/tampilan_agama';
		$isi['judul']			='Data';
		$isi['sub_judul']		= 'Agama';
		$isi['id_agama']		= "";
		$isi['nama_agama']		= "";
		$this->load->view('tampilan_home',$isi);
	}

	public function edit()
	{
		$this->model_security->getsecurity();
		$isi['content']			= 'agama/tampilan_agama';
		$isi['judul']			='Data';
		$isi['sub_judul']		= 'Edit Agama';
		$isi['data']			= $this->db->get('agama');
		$key = $this->uri->segment(3);
		$this->db->where('id_agama',$key);
		$query = $this->db->get('agama');
		if($query->num_rows()>0)
		{
			foreach ($query->result() as $row)

			{
					$isi['id_agama']			= $row->id_agama;
					$isi['nama_agama']			= $row->nama_agama;
					
			}
		}
		else
		{
					$isi['id_agama']			= "";
					$isi['nama_agama']			= "";
					
		}

		$this->load->view('tampilan_home',$isi);

	}

		public function simpan()
	{
		$this->model_security->getsecurity();
		$key = $this->input->post('id_agama');
		$data['id_agama']			=		$this->input->post('id_agama');
		$data['nama_agama']			=		$this->input->post('nama_agama');
		
		$this->load->model('model_agama');
		$query = $this->model_agama->getdata($key);
		if($query->num_rows()>0)
		{
			$this->model_agama->getupdate($key,$data);
			$this->session->set_flashdata('info','Data sukses di update');
		}
		else
		{
		$this->model_agama->getinsert($data);
		$this->session->set_flashdata('info','Data sukses di simpan');
	}
	redirect('agama');

}
public function delete($id)
{
$this->model_security->getsecurity();
	$this->db->where('id_agama', $id);
	$this->db->delete('agama');

	$this->db->where('id_agama', $id);
	$this->db->delete('siswa');
		redirect('agama');
}
}