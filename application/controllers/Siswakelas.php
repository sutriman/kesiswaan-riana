<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiswaKelas extends CI_Controller {

	
	public function index($aksi=null)
	{

		if ($aksi == 'cari') {
			$this->db->select('*');
		$this->db->join('siswa', 'siswa.NISN = siswa_kelas.NISN');
		$this->db->join('tahun_ajaran', 'tahun_ajaran.id_tahun_ajaran = siswa_kelas.id_tahun_ajaran');
		$this->db->join('kelas', 'kelas.id_kelas = siswa_kelas.id_kelas');
		$this->db->where('tahun_ajaran.id_tahun_ajaran', $this->input->post('tahun'));
		$this->db->where('kelas.id_kelas', $this->input->post('id_kelas'));
		$d = $this->db->get('siswa_kelas')->result();

		$isi['hasil_cari'] = $d;
		$isi['tahun_l'] = $this->input->post('tahun') + 1;
		$isi['kelas_l'] = $this->input->post('id_kelas');

		// echo print_r($this->input->post());
		// echo print_r($isi['hasil_cari']);
		} else {
			$isi['hasil_cari'] = null;
			$isi['tahun_l'] = null;
			$isi['kelas_l'] = null;
		}
		$this->load->model('Model_siswakelas');
		$this->model_security->getsecurity();

		$isi['content']			= 'siswa_kelas/tampil_siswakelas';
		$isi['judul']			='Data';
		$isi['sub_judul']		= 'Kenaikan Kelas';
		$isi['kelas']			= $this->db->get('kelas')->result();
		$isi['tahun_ajaran']	= $this->db->get('tahun_ajaran')->result();	
		$tahun = $this->input->post('tahun_id');
		$kelas = $this->input->post('kelas');
		// $isi['data']			= $this->db->get('siswa_kelas');
		$isi['data']		= $this->Model_siswakelas->get_data($tahun,$kelas);
		$this->load->view('tampilan_home',$isi);
	}


	public function pindah_t_ajaran() 
	{

		$kelas = $this->input->post('id_kelas');
		$tahun = $this->input->post('tahun_baru');

		echo $kelas;

		//Update status lulus tahun lama
		foreach ($this->input->post('id_siswa') as $key => $value) {

		$data = array(
				'status_lulus' => 'naik'
				);

			$this->db->where('NISN', $value);
			$this->db->where('id_tahun_ajaran', $tahun+1);
			$this->db->update('siswa_kelas', $data);
		}

		//Insert siswa lulus ke tahun baru
		foreach ($this->input->post('id_siswa') as $key => $value) {

		$data = array(
				'NISN' => $value,
				'id_tahun_ajaran' => $tahun,
				'id_kelas' => $kelas,
				'status_lulus' => ''
				);

			$this->db->insert('siswa_kelas', $data);
		}



			// $isi['content']			= 'siswa_kelas/tampil_siswakelas';
			// $this->load->view('tampilan_home',$isi);
	}

public function delete()
{
$this->model_security->getsecurity();
$this->load->model('model_siswakelas');

		$key = $this->uri->segment(3);
		$this->db->where('NISN',$key);
		$query = $this->db->get('siswa_kelas');
		if($query->num_rows()>0)
		{
					$this->model_siswakelas->getdelete($key);
		}
		redirect('siswa_kelas');
}
}