<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {

	
	public function index()
	{	
		$this->load->model('Model_siswa');
		$this->model_security->getsecurity();
		$isi['content'] = 'siswa/tampil_datasiswa';
		$isi['judul']		='Data';
		$isi['sub_judul']	= 'Siswa';
		$isi['data']		= $this->Model_siswa->get_data()->result();
		$this->load->view('tampilan_home',$isi);

	}

	public function tambah()
	{
		$this->model_security->getsecurity();
		$this->load->model('Model_siswa');
		$isi['content']			= 'siswa/form_tambahsiswa';
		$isi['judul']			='Data';
		$isi['edit']			= null;
		$isi['sub_judul']		= 'Tambah Siswa';
		$isi['agamaa']			= $this->db->get('agama')->result();
		$isi['kelass']			= $this->db->get('kelas')->result();
		$isi['kabupaten']		= $this->db->get('kabupaten')->result();
		$isi['kecamatan']		= $this->db->get('kecamatan')->result();
		$isi['tahun_ajaran']	= $this->db->get('tahun_ajaran')->result();
					$isi['NISN']				= "";
					$isi['nama_siswa']			= "";
					$isi['j_kelamin']			= "";
					$isi['tmpt_lhr']			= "";
					$isi['tgl_lhr'] 			= "";
					$isi['id_agama']			= "";
					$isi['asal_sekolah']		= "";
					$isi['alamat_sekarang']		= "";
					$isi['id_kabupaten']		= "";
					$isi['id_kecamatan']		= "";
					$isi['status']				= "";
					$isi['nama_ortu']			= "";
					$isi['no_telp'] 			= "";
					$isi['alamat']				= "";
					$isi['id_kelas']			= "";
					$isi['id_tahun_ajaran']		= "";

		$this->load->view('tampilan_home',$isi);
		}

public function edit()
	{
		$this->model_security->getsecurity();
		$isi['edit'] = true;
		$isi['content']			= 'siswa/form_tambahsiswa';
		$isi['judul']			= 'Data';
		$isi['sub_judul']		= 'Edit Siswa';
		$isi['agamaa']			= $this->db->get('agama')->result();
		$isi['kelass']			= $this->db->get('kelas')->result();
		$isi['kabupaten']		= $this->db->get('kabupaten')->result();
		$isi['kecamatan']		= $this->db->get('kecamatan')->result();
		$isi['tahun_ajaran']	= $this->db->get('tahun_ajaran')->result();
		$isi['data']			= $this->db->get('siswa');
		$key = $this->uri->segment(3);
		$this->db->where('siswa_kelas.NISN',$key);
		//$this->db->where('agama.id_agama',$key);
		$this->db->join('tahun_ajaran', 'siswa_kelas.id_tahun_ajaran = tahun_ajaran.id_tahun_ajaran');
		$this->db->join('kelas', 'kelas.id_kelas = siswa_kelas.id_kelas');
		$this->db->join('siswa', 'siswa.NISN = siswa_kelas.NISN');
		
		$query = $this->db->get('siswa_kelas');
		// $query = $this->db->get('agama');
		// echo print_r($query->row()->NISN);
		// if($query->num_rows() == 1)
		// {
		// 	foreach ($query->result() as $row)

		// 	{
		// 			$isi['NISN']				= $row->NISN;
		// 			$isi['nama_siswa']			= $row->nama_siswa;
		// 			$isi['j_kelamin']			= $row->j_kelamin;
		// 			$isi['tmpt_lhr']			= $row->tmpt_lhr;
		// 			$isi['tgl_lhr']				= $row->tgl_lhr;
		// 			$isi['id_agama']			= $row->id_agama;
		// 			$isi['asal_sekolah']		= $row->asal_sekolah;
		// 			$isi['alamat_sekarang']		= $row->alamat_sekarang;
		// 			$isi['dalam_luar_kota']		= $row->dalam_luar_kota;
		// 			$isi['status']				= $row->status;
		// 			$isi['nama_ortu']			= $row->nama_ortu;
		// 			$isi['no_telp'] 			= $row->no_telp;
		// 			$isi['alamat']				= $row->alamat;
		// 			// $isi['id_kelas']			= $row->id_kelas;
		// 			// $isi['id_tahun_ajaran']		= $row->id_tahun_ajaran;
		// 	}
		// }
		// else
		// {
		// 			$isi['NISN']				= "";
		// 			$isi['nama_siswa']			= "";
		// 			$isi['j_kelamin']			= "";
		// 			$isi['tmpt_lhr']			= "";
		// 			$isi['tgl_lhr'] 			= "";
		// 			$isi['id_agama']			= "";
		// 			$isi['asal_sekolah']		= "";
		// 			$isi['alamat_sekarang']		= "";
		// 			$isi['dalam_luar_kota']		= "";
		// 			$isi['status']				= "";
		// 			$isi['nama_ortu']			= "";
		// 			$isi['no_telp'] 			= "";
		// 			$isi['alamat']				= "";
		// 			$isi['id_kelas']			= "";
		// 			$isi['id_tahun_ajaran']		= "";
		// }

					$isi['NISN']				= $query->row()->NISN;
					$isi['nama_siswa']			= $query->row()->nama_siswa;
					$isi['j_kelamin']			= $query->row()->j_kelamin;
					$isi['tmpt_lhr']			= $query->row()->tmpt_lhr;
					$isi['tgl_lhr'] 			= $query->row()->tgl_lhr;
					$isi['id_agama']			= $query->row()->id_agama;
					$isi['asal_sekolah']		= $query->row()->asal_sekolah;
					$isi['alamat_sekarang']		= $query->row()->alamat_sekarang;
					$isi['id_kabupaten']		= $query->row()->id_kabupaten;
					$isi['id_kecamatan']		= $query->row()->id_kecamatan;
					$isi['nama_ortu']			= $query->row()->nama_ortu;
					$isi['no_telp'] 			= $query->row()->no_telp;
					$isi['alamat']				= $query->row()->alamat;
					$isi['id_kelas']			= $query->row()->id_kelas;
					$isi['id_tahun_ajaran']		= $query->row()->id_tahun_ajaran;

	
		$this->load->view('tampilan_home',$isi);

	}
		public function simpan($edit = null)
		{
		$key = $this->input->post('NISN');
		
		
		$this->load->model('Model_siswa');
		$query = $this->Model_siswa->getdata($key);
		// if($query->num_rows()>0)
		

		// 	{
		// 	$this->Model_siswa->getupdate($key,$data);
			// $this->session->set_flashdata('info','Data sukses di update');
		// }
		// else
		// {
		if ($edit == true) {

		$data['nama_siswa']			= $this->input->post('nama_siswa');
		$data['j_kelamin']			= $this->input->post('j_kelamin');
		$data['tmpt_lhr']			= $this->input->post('tmpt_lhr');
		$data['tgl_lhr']			= $this->input->post('tgl_lhr');
		$data['id_agama']			= $this->input->post('id_agama');
		$data['asal_sekolah']		= $this->input->post('asal_sekolah');
		$data['alamat_sekarang']	= $this->input->post('alamat_sekarang');
		$data['id_kabupaten']		= $this->input->post('kabupaten');	
		$data['id_kecamatan']		= $this->input->post('kecamatan');	
		$data['nama_ortu']			= $this->input->post('nama_ortu');
		$data['no_telp']			= $this->input->post('no_telp');
		$data['alamat']				= $this->input->post('alamat');
		$data2['id_kelas']			= $this->input->post('id_kelas');
		$data2['id_tahun_ajaran']	= $this->input->post('id_tahun_ajaran');

			$this->Model_siswa->getupdate($key, $data);
			$this->Model_siswa->getupdate2($key, $data2);

				$this->session->set_flashdata('info','Data sukses di simpan');
		} else {

		$data['NISN']				= $this->input->post('NISN');
		$data['nama_siswa']			= $this->input->post('nama_siswa');
		$data['j_kelamin']			= $this->input->post('j_kelamin');
		$data['tmpt_lhr']			= $this->input->post('tmpt_lhr');
		$data['tgl_lhr']			= $this->input->post('tgl_lhr');
		$data['id_agama']			= $this->input->post('id_agama');
		$data['asal_sekolah']		= $this->input->post('asal_sekolah');
		$data['alamat_sekarang']	= $this->input->post('alamat_sekarang');
		$data['id_kabupaten']		= $this->input->post('kabupaten');	
		$data['id_kecamatan']		= $this->input->post('kecamatan');	
		$data['nama_ortu']			= $this->input->post('nama_ortu');
		$data['no_telp']			= $this->input->post('no_telp');
		$data['alamat']				= $this->input->post('alamat');
		$data2['NISN']				= $this->input->post('NISN');
		$data2['id_kelas']			= $this->input->post('id_kelas');
		$data2['id_tahun_ajaran']	= $this->input->post('id_tahun_ajaran');

		$this->Model_siswa->getinsert($data);
		$this->Model_siswa->getinsert2($data2);
		// }
		
		$this->session->set_flashdata('info','Data sukses di simpan');
	
	
	}
	redirect('siswa');
}

	public function delete($id)
	{
	$this->model_security->getsecurity();

	$this->db->where('NISN', $id);
	$this->db->delete('siswa_kelas');

	$this->db->where('NISN', $id);
	$this->db->delete('siswa');

	// $this->load->model('model_siswa');

	// 	$key = $this->uri->segment(3);
	// 	$this->db->where('NISN',$key);
	// 	$query = $this->db->get('siswa');
	// 	if($query->num_rows()>0)
	// 	{
	// 				$this->model_siswa->getdelete($key);
	// 	}
		redirect('siswa');
	}

}
