<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
  function __construct(){
    parent::__construct();
    $this->load->helper(array('url','form'));
    $this->load->model('model_laporan');
    $this->load->library('pdf');
  }
	
	public function laporan1() {
		//menampilkan jumlah siswa berdasarkan jenis kelamin
    $isi['tahun_ajar'] = $this->input->post('tahun');
		$this->load->model('model_laporan');
				$this->model_security->getsecurity();
		$isi['content']			= 'laporan/laporan1';
		$isi['judul']			  ='Laporan';
		$isi['sub_judul']		= 'Jumlah siswa berdasarkan jenis kelamin';
    $isi['tahun_ajaran']= $this->db->get('tahun_ajaran')->result();
		$isi['data']			  = $this->model_laporan->laporan1($isi['tahun_ajar']);
		$this->load->view('tampilan_home',$isi);
		// print_r(expression)
	}
	public function laporan2() {
		//menampilkan jumlah siswa berdasarkan jenis kelamin
    $isi['tahun_ajar2'] = $this->input->post('tahun');
		$this->load->model('model_laporan');
				$this->model_security->getsecurity();
		$isi['content']			= 'laporan/laporan2';
		$isi['judul']			  ='Laporan';
		$isi['sub_judul']		= 'Jumlah siswa berdasarkan tempat tinggal';
	   $isi['tahun_ajaran'] = $this->db->get('tahun_ajaran')->result();
    $isi['data']         = $this->model_laporan->laporan2($isi['tahun_ajar2']);
		$this->load->view('tampilan_home',$isi);
	}
	public function laporan3() {
		//menampilkan jumlah siswa berdasarkan jenis kelamin
    $isi['tahun_ajar3'] = $this->input->post('tahun');
		$this->load->model('model_laporan');
				$this->model_security->getsecurity();
		$isi['content']			= 'laporan/laporan3';
		$isi['judul']			='Laporan';
		$isi['sub_judul']		= 'Jumlah siswa berdasarkan agama';
    $isi['tahun_ajaran']   = $this->db->get('tahun_ajaran')->result();
		$isi['data']			= $this->model_laporan->laporan3($isi['tahun_ajar3']);
    
		$this->load->view('tampilan_home',$isi);
	}
	public function laporan4() {
		//menampilkan jumlah siswa berdasarkan jenis kelamin
    $isi['tahun_ajar'] = $this->input->post('tahun');
    $this->load->model('model_laporan');
    $isi['tahun_lahir'] = $this->model_laporan->tahun_lahir();
		
		$this->model_security->getsecurity();
		
    $isi['content']			= 'laporan/laporan4';
		$isi['judul']			='Laporan';
		$isi['sub_judul']		= 'Jumlah siswa berdasarkan tahun kelahiran';
		$isi['data']			= $this->model_laporan->laporan4($isi['tahun_ajar']);
    $isi['tahun_ajaran']   = $this->db->get('tahun_ajaran')->result();
		$this->load->view('tampilan_home',$isi);
	}
public function laporan5() {
    //menampilkan jumlah siswa berdasarkan jenis kelamin
    $isi['tahun_ajar']     = $this->input->post('tahun');
    $this->load->model('model_laporan');
    $isi['tahun_lahir2']    = $this->model_laporan->tahun_lahir2();
    $this->model_security->getsecurity();
    
    $isi['content']         = 'laporan/laporan5';
    $isi['judul']           ='Laporan';
    $isi['sub_judul']       = 'Jumlah siswa berdasarkan umur';
    $isi['data']            = $this->model_laporan->laporan5($isi['tahun_ajar']);
    $isi['tahun_ajaran']    = $this->db->get('tahun_ajaran')->result();
    $this->load->view('tampilan_home',$isi);
  }

	public function cetak_laporan1() {


	// tcpdf config
 	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT,PDF_PAGE_FORMAT,  true, 'UTF-8', false);   
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->SetDisplayMode('real');
        $pdf->setPageOrientation('P');
        $pdf->SetMargins(20, 20, 20, false);
        // add a page
        $pdf->AddPage();
        // write
       

        $data['siswa']= $this->model_laporan->laporan1(null);
        $hasil = '';
		//menampilkan gambar untuk header
        // $hasil.='<img src="'.base_url().'assets/img/sma8.png" height="170px" align="center"> <br>';

// test Cell stretching
$pdf->Cell(0, 0, 'PEMERINTAH KOTA YOGYAKARYA', 0, 0, 'C', 0, 0);
$pdf->Cell(0, 0, 'DINAS PENDIDIKAN', 1, 1, 'C', 0, 1);
$pdf->Cell(0, 0, 'SMA NEGERI 8 YOGYAKARTA', 0, 0, 'C', 0, 1);
$pdf->Cell(0, 0, 'Jl. Sidobali 1 Muja-Muju Telp.(0274) 513493, Fax.(0274) 580207 Yogyakarta 55165', 1, 1, 'C', 0, '', 1);
$pdf->Cell(0, 0, 'E-mail : sman8yogya@yahoo.com, Website : http://www.sman8yogya.sch.id', 1, 1, 'C', 0, '', 4);


        $hasil.='<hr><br>';
        $hasil.='<br>';
        $hasil.='<center><H2>LAPORAN DATA JUMLAH SISWA BERDASARKAN JENIS KELAMIN</H2></center>';
        $hasil.='</br>';
        $hasil.='<table border="1" cellspacing="1" align="center"> <tr>
        	<th>No</th>
			<th>Kode</th>	
			<th>Nama Kelas</th>
			<th>Jumlah Laki-Laki</th>
			<th>Jumlah Perempuan</th></tr>';
        $no=1;
           foreach ($data['siswa']->result() as $key) {
           		$id_kelas= $key->id_kelas;
           		$nama_kelas= $key->nama_kelas;
           		$jmllaki= $key->jumlahlaki;
           		$jmlp= $key->jumlahperempuan;
           		$hasil.='<tr>
           					<td>'.$no++.'</td>
           					<td align="left">&nbsp;&nbsp;'.$id_kelas.'</td>
           					<td>'.$nama_kelas.'</td>
           					<td>'.$jmllaki.'</td>
           					<td>'.$jmlp.'</td>

           				</tr> 
           				';

           }
       
 $hasil.='</table>';

        $pdf->writeHTML($hasil, true, false, true, false, '');

        // output (D : download, I : view)
        $pdf->Output('laporan1.pdf', 'I');
    }
public function cetak_laporan2() {
	// tcpdf config
 	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT,PDF_PAGE_FORMAT,  true, 'UTF-8', false);   
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->SetDisplayMode('real');
        $pdf->setPageOrientation('P');
        $pdf->SetMargins(20, 20, 20, false);
        // add a page
        $pdf->AddPage();
        // write
        
        $data['siswa']= $this->model_laporan->laporan2(null);
        $hasil = '';
		//menampilkan gambar untuk header
        $hasil.='<img src="'.base_url().'assets/img/sma8.png" height="170px" align="center"> <br>';
        $hasil.='<hr><br>';
        $hasil.='<br>';
        $hasil.='<center><H2>DATA JUMLAH SISWA BERDASARKAN TEMPAT TINGGAL</H2></center>';
         $hasil.='<center><H2>SMAN 8 YOGYAKARYA</H2></center>';
        $hasil.='</br>';
        $hasil.='<table border="1" cellspacing="0" align="center"> <tr>
        	<th>No</th>
			<th>Kode</th>
			<th>Nama Kelas</th>
			<th>Jumlah Dalam Kota</th>
			<th>Jumlah Luar Kota</th></tr>';
        $no=1;
           foreach ($data['siswa']->result() as $key) {
           		$id_kelas= $key->id_kelas;
           		$nama_kelas= $key->nama_kelas;
           		$dlm= $key->dalamkota;
           		$luar= $key->luarkota;
           		$hasil.='<tr>
           					<td>'.$no++.'</td>
           					<td align="left">&nbsp;&nbsp;'.$id_kelas.'</td>
           					<td>'.$nama_kelas.'</td>
           					<td>'.$dlm.'</td>
           					<td>'.$luar.'</td>

           				</tr> 
           				';

           }
       
 $hasil.='</table>';

        $pdf->writeHTML($hasil, true, false, true, false, '');

        // output (D : download, I : view)
        $pdf->Output('laporan2.pdf', 'I');
    }
public function cetak_laporan3() {
	// tcpdf config
 	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT,PDF_PAGE_FORMAT,  true, 'UTF-8', false);   
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->SetDisplayMode('real');
        $pdf->setPageOrientation('P');
        $pdf->SetMargins(20, 20, 20, false);
        // add a page
        $pdf->AddPage();
        // write
        
        $data['siswa']= $this->model_laporan->laporan3(null);
        $hasil = '';
		//menampilkan gambar untuk header
        // $hasil.='<img src="'.base_url().'assets/img/kop.png" height="170px" align="center"> <br>';
        $hasil.='<hr><br>';
        $hasil.='<br>';
        $hasil.='<center><H2>DATA JUMLAH SISWA BERDASARKAN AGAMA</H2></center>';
         $hasil.='<center><H2>SMAN 8 YOGYAKARYA</H2></center>';
        $hasil.='</br>';
        $hasil.='<table border="1" cellspacing="0" align="center"> <tr>
        	<th>No</th>
			<th>Kode</th>
			<th>Nama Kelas</th>
			<th>Islam</th>
			<th>Kristen</th>
			<th>Katholik</th>
			<th>Hindu</th>
			<th>Budha</th>
			<th>Lainnya</th>
			</tr>';
        $no=1;
           foreach ($data['siswa']->result() as $key) {
           		$id_kelas= $key->id_kelas;
           		$nama_kelas= $key->nama_kelas;
           		$i= $key->islam;
           		$kr= $key->kristen;
           		$ka= $key->katholik;
           		$h= $key->hindu;
           		$b= $key->budha;
           		$l= $key->lainnya;
           		$hasil.='<tr>
           					<td>'.$no++.'</td>
           					<td align="left">&nbsp;&nbsp;'.$id_kelas.'</td>
           					<td>'.$nama_kelas.'</td>
           					<td>'.$i.'</td>
           					<td>'.$kr.'</td>
           					<td>'.$ka.'</td>
           					<td>'.$h.'</td>
           					<td>'.$b.'</td>
           					<td>'.$l.'</td>

           				</tr>';

           }
       
 $hasil.='</table>';

        $pdf->writeHTML($hasil, true, false, true, false, '');

        // output (D : download, I : view)
        $pdf->Output('laporan3.pdf', 'I');
    }

    public function cetak_laporan4() {
	// tcpdf config
 	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT,PDF_PAGE_FORMAT,  true, 'UTF-8', false);   
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->SetDisplayMode('real');
        $pdf->setPageOrientation('P');
        $pdf->SetMargins(20, 20, 20, false);
        // add a page
        $pdf->AddPage();
        // write
        $isi['tahun_ajar'] = $this->input->post('tahun');
        $tahun_lahir = $this->model_laporan->tahun_lahir(null);
        $data['siswa']= $this->model_laporan->laporan4(null);
        $data['tahun_lahir']= $this->model_laporan->tahun_lahir(null);
        $isi['data']      = $this->model_laporan->laporan4($isi['tahun_ajar']);
        $hasil = '';
		//menampilkan gambar untuk header
        // $hasil.='<img src="'.base_url().'assets/img/kop.png" height="170px" align="center"> <br>';
        $hasil.='<hr><br>';
        $hasil.='<br>';
        $hasil.='<center><H2>DATA JUMLAH SISWA BERDASARKAN TAHUN KELAHIRAN</H2></center>';
        $hasil.='<center><H2>SMAN 8 YOGYAKARYA</H2></center>';
        $hasil.='</br>';
        $hasil.='<table border="1" cellspacing="0" align="center"> <tr>
        	<th>No</th>
			<th>Kode</th>
			<th>Nama Kelas</th>';
			 foreach ($tahun_lahir as $key => $value) {
              $thun= $value->thn_lhr;
              $hasil.='<th>'.$thun.'</th>';
      } 
      $hasil.= '</tr>';

       $no=1;
           foreach ($data['siswa']->result() as $row) {
              $id_kelas= $row->id_kelas;
              $nama_kelas= $row->nama_kelas;
              $hasil.='<tr>
                    <td>'.$no++.'</td>
                    <td align="left">&nbsp;&nbsp;'.$id_kelas.'</td>
                    <td>'.$nama_kelas.'</td>';
                    foreach ($tahun_lahir as $key => $value) {
                      $a = $this->model_laporan->hitung_siswa($value->thn_lhr, $row->id_kelas, '');
                      foreach ($a as $key => $value) {
                       $jml = $value->jumlah;
                       $hasil.='<td>'.$jml.'</td>';
                      }

                  } 
                  
                  $hasil.='</tr>';

           }

      $hasil.='</table>';

        $pdf->writeHTML($hasil, true, false, true, false, '');

        // output (D : download, I : view)
        $pdf->Output('laporan4.pdf', 'I');
      }

    }