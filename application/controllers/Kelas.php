<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller {

	
	public function index()
	{
		$this->model_security->getsecurity();
		$isi['content']			= 'kelas/tampil_datakelas';
		$isi['judul']			='Data';
		$isi['sub_judul']		= 'Kelas';
		$isi['id_kelas']		= "";
		$isi['nama_kelas']		= "";
		$isi['data']			= $this->db->get('kelas');
		$this->load->view('tampilan_home',$isi);
	}
	
		public function tambah()
	{
		$this->model_security->getsecurity();
		$isi['content']			= 'kelas/tampil_datakelas';
		$isi['judul']			='Data';
		$isi['sub_judul']		= 'Kelas';
		$isi['id_kelas']		= "";
		$isi['nama_kelas']		= "";
		$this->load->view('tampilan_home',$isi);
	}

	public function edit()
	{
		$this->model_security->getsecurity();
		$isi['content']			= 'kelas/tampil_datakelas';
		$isi['judul']			='Data';
		$isi['sub_judul']		= 'Edit Kelas';
		$isi['data']			= $this->db->get('kelas');
		$key = $this->uri->segment(3);
		$this->db->where('id_kelas',$key);
		$query = $this->db->get('kelas');
		if($query->num_rows()>0)
		{
			foreach ($query->result() as $row)

			{
					$isi['id_kelas']			= $row->id_kelas;
					$isi['nama_kelas']			= $row->nama_kelas;
			}
		}
		else
		{
					$isi['id_kelas']			= "";
					$isi['nama_kelas']			= "";
					
		}

		$this->load->view('tampilan_home',$isi);

	}

		public function simpan()
	{
		$this->model_security->getsecurity();
		$key = $this->input->post('id_kelas');
		$data['id_kelas']			=		$this->input->post('id_kelas');
		$data['nama_kelas']			=		$this->input->post('nama_kelas');
		
		$this->load->model('model_kelas');
		$query = $this->model_kelas->getdata($key);
		if($query->num_rows()>0)
		{
			$this->model_kelas->getupdate($key,$data);
			$this->session->set_flashdata('info','Data sukses di update');
		}
		else
		{
		$this->model_kelas->getinsert($data);
		$this->session->set_flashdata('info','Data sukses di simpan');
	}
	redirect('kelas');

}
public function delete($id)
{
$this->model_security->getsecurity();
	$this->db->where('id_kelas', $id);
	$this->db->delete('siswa_kelas');

	$this->db->where('id_kelas', $id);
	$this->db->delete('kelas');
		redirect('kelas');
}
}