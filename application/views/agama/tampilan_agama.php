
<?php
$info = $this->session->flashdata('info');
if(!empty($info))
{
	echo $info;
}
?>



		<form class="form-horizontal" method="POST" action="<?php echo base_url();?>index.php/agama/simpan" onsubmit="return cekform();">
<div class="col-xs-12">
										<h3 class="header smaller lighter blue">Tambah Agama</h3>

										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
	<div class="form-group">
	<label for="id_kelas" class="control-label col-sm-3">Kode Agama</label>
	<div class="col-sm-3">
		<input type="text" name="id_agama" id="id_agama" class="form-control" placeholder="Masukkan Kode Agama"  value="<?php echo $id_agama; ?>"> 
		</div>
		</div>

	<div class="form-group">
	<label for="nama_kelas" class="control-label col-sm-3">Nama Agama</label>
	<div class="col-sm-3">
		<input type="text" name="nama_agama" id="nama_agama" class="form-control" placeholder="Masukkan Nama Agama" value="<?php echo $nama_agama; ?>"> 
		</div>
		</div>


	
		
		
		<div>
		&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
			<button type="submit" class="btn btn-primary btn-small">Simpan</button>
			
			
			
</form>

<div class="col-xs-12">
										<h3 class="header smaller lighter blue">Table Data Agama</h3>

										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
	

			<table id="dynamic-table" class="table table-striped table-bordered table-hover">
		<thead>

<tr>
<th>No</th>
<th>Kode Agama</th>
<th>Nama Agama</th>
<th>Aksi</th>
</tr>
			</thead>
			<tbody>
			<tr>
			<?php 
			$no = 1;
			foreach ($data->result() as $row ) { 
			?>
			<tr>

<td><?php echo $no++; ?></td>
<td><?php echo $row->id_agama; ?></td>
<td><?php echo $row->nama_agama; ?></td>

<td>
<div class="hidden-sm hidden-xs action-buttons">
					
		<a class="green" href="<?php echo base_url()?>index.php/agama/edit/<?php echo $row->id_agama; ?>">
		<i class="ace-icon fa fa-pencil bigger-130"></i>
		</a>


&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="red" href="<?php echo base_url()?>index.php/agama/delete/<?php echo $row->id_agama; ?>" onclick="return confirm('anda yakin akan menghapus data ini?'); ">
		<i class="ace-icon fa fa-trash-o bigger-130"></i>
		</a>
		</div>
	
</td>

</tr>
			<?php } ?>
	</tbody>
	</div>
</div>
</table>	
