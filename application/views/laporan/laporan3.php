<?php



?>
<form class="form-search" action="<?php echo site_url('laporan/laporan3');  ?>" method="post">
      <table class="table-form" width="100%">

                                    <div class="widget-main">
                                      
                                          <div class="row">

                                             <div class="col-sm-3">
                                                <div class="input-group">
                                                   <div class="col-sm-6">
                                                      <label>Tahun Ajaran</label>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <!-- <input type="hidden" value="1" name="tahun_id"> -->
                                                      <select name="tahun" id="id_kelas" data-placeholder="--Pilih--">
                                                      <option value="">-- Pilih --</option>
                                                      <?php foreach ($tahun_ajaran as $a) { ?>
                                                      <option value="<?php echo $a->id_tahun_ajaran; ?>"><?php echo $a->tahun_ajaran; ?></option>
                                                      <?php } ?>
                                                   </select>
                                                      <!-- <input type="text"  name="tahun" class="form-control search-query" placeholder="Cari Data" /> -->
                                                   
                                                   </div>
                                                </div>
                                                   
                                             </div>

                                                   <div class="col-sm-2">
                                                      <span class="input-group-btn">
                                                      <button type="submit" class="btn btn-purple btn-sm">
                                                         <span class="ace-icon icon-on-right bigger-110"></span>
                                                         Cari
                                                      </button>
                                                      </span>
                                                   </div>
                                                   
                                             </div>

                                          </div>

   </form>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>

<th>No</th>
<th>Kode</th>
<th>Nama Kelas</th>
<th>Islam</th>
<th>Kristen</th>
<th>Katholik</th>
<th>Hindu</th>
<th>Budha</th>
</tr>
</thead>
     
     <tbody>
      <tr>
      <?php 
      $no = 1;
      $jumlah = 0;
      $jumlah1 = 0;
      $jumlah2 = 0;
      $jumlah3 = 0;
      $jumlah4 = 0;
      foreach ($data->result() as $row ) { 
      ?>
      <tr>

<td><?php echo $no++; ?></td>
<td><?php echo $row->id_kelas; ?></td>
<td><?php echo $row->nama_kelas; ?></td>  
<td><?php echo $row->islam; ?></td> 
<td><?php echo $row->kristen; ?></td> 
<td><?php echo $row->katholik; ?></td> 
<td><?php echo $row->hindu; ?></td> 
<td><?php echo $row->budha; ?></td> 
<?php
$jumlah = $jumlah + $row->islam; ?>
<?php
$jumlah1 = $jumlah1 + $row->kristen; ?>
<?php
$jumlah2 = $jumlah2 + $row->katholik; ?>
<?php
$jumlah3 = $jumlah3 + $row->hindu; ?>
<?php
$jumlah4 = $jumlah4 + $row->budha; ?>
   

   
<?php } 

?>
<tr>
   
   <th colspan="3" style="text-align: center;">TOTAL</th>
   <td><?php echo $jumlah ;?></td>
   <td><?php echo $jumlah1 ;?></td>
   <td><?php echo $jumlah2 ;?></td>
   <td><?php echo $jumlah3 ;?></td>
   <td><?php echo $jumlah4 ;?></td>
</tr>

 </div>


 <a href="<?php echo base_url();?>index.php/laporan/cetak_laporan3" class="btn btn-info ace-icon fa fa-print  align-top bigger-125 icon-on-right">Cetak</a>
    
      
  </tbody>
</table>    
  </div>
</div>
  
   <script type="text/javascript">
      
               jQuery(function($) {
$('#id_kelas').select2()
});
   </script>
  