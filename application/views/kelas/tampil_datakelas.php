
<?php
$info = $this->session->flashdata('info');
if(!empty($info))
{
	echo $info;
}
?>


<form class="form-horizontal" method="POST" action="<?php echo base_url();?>index.php/kelas/simpan" onsubmit="return cekform();">
<div class="col-xs-12">
										<h3 class="header smaller lighter blue">Tambah Data Kelas</h3>

										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
	<div class="form-group">
	<label for="id_kelas" class="control-label col-sm-3">Kode Kelas</label>
	<div class="col-sm-3">
		<input type="text" name="id_kelas" id="id_kelas" class="form-control" placeholder="Masukkan Kode Kelas"  value="<?php echo $id_kelas; ?>"> 
		</div>
		</div>

	<div class="form-group">
	<label for="nama_kelas" class="control-label col-sm-3">Nama Kelas</label>
	<div class="col-sm-3">
		<input type="text" name="nama_kelas" id="nama_kelas" class="form-control" placeholder="Masukkan Nama Kelas" value="<?php echo $nama_kelas; ?>"> 
		</div>
		</div>


		<!-- <div class="form-group">
	<label for="status" class="control-label col-sm-3">Status</label>
	<div class="space-3"></div>

	<div class="form-group">

	<div class="col-xs-3">
	<div>
																		
	<input name="status" value="1" type="radio" class="ace" />
	<span class="lbl"> Aktif</span>
	
	</div>

		<div>
																		
		<input name="status" value="2" type="radio" class="ace" />
		<span class="lbl"> Tidak Aktif</span>
			</label>
			</div>
		</div>
		</div>
																
		</div> -->
		
		
		<div>
		&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
			<button type="submit" class="btn btn-primary btn-small">Simpan</button>
			
			
			
</form>

<div class="col-xs-12">
										<h3 class="header smaller lighter blue">Table Data Kelas</h3>

										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
	

			<table id="dynamic-table" class="table table-striped table-bordered table-hover">
		<thead>

<tr>
<th>No</th>
<th>Kode</th>
<th>Nama Kelas</th>
<th>Aksi</th>
</tr>
			</thead>
			<tbody>
			<tr>
			<?php 
			$no = 1;
			foreach ($data->result() as $row ) { 
			?>
			<tr>

<td><?php echo $no++; ?></td>
<td><?php echo $row->id_kelas; ?></td>
<td><?php echo $row->nama_kelas; ?></td>
<!-- <td>
<?php 
if ($row->status=='1') {
	echo 'aktif';
} 
elseif ($row->status=='2'){
		echo 'tidak aktif';
}
else{
	echo '-';
}

?>
	
</td> -->
<td>
<div class="hidden-sm hidden-xs action-buttons">
					
		<a class="green" href="<?php echo base_url()?>index.php/kelas/edit/<?php echo $row->id_kelas; ?>">
		<i class="ace-icon fa fa-pencil bigger-130"></i>
		</a>


&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="red" href="<?php echo base_url()?>index.php/kelas/delete/<?php echo $row->id_kelas; ?>" onclick="return confirm('anda yakin akan menghapus data ini?'); ">
		<i class="ace-icon fa fa-trash-o bigger-130"></i>
		</a>
		</div>
	
</td>

</tr>
			<?php } ?>
	</tbody>
	</div>
</div>
</table>		
	

		<script type="text/javascript">
			jQuery(function($) {
				
				var oTable1 = $('#dynamic-table').dataTable();
				
			
			
			
				//initiate TableTools extension
				var tableTools_obj = new $.fn.dataTable.TableTools( {
					"sSwfPath": "../assets/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf", //in Ace demo ../assets will be replaced by correct assets path
					
					"sRowSelector": "td:not(:last-child)",
					"sRowSelect": "multi",
					"fnRowSelected": function($) {
						//check checkbox when row is selected
						try { $(row).find('input[type=checkbox]').get(0).checked = true }
						catch(e) {}
					},
					"fnRowDeselected": function($) {
						//uncheck checkbox
						try { $(row).find('input[type=checkbox]').get(0).checked = false }
						catch(e) {}
					},
			
					"sSelectedClass": "success",
			        "aButtons": [
						{
							"sExtends": "copy",
							"sToolTip": "Copy to clipboard",
							"sButtonClass": "btn btn-white btn-primary btn-bold",
							"sButtonText": "<i class='fa fa-copy bigger-110 pink'></i>",
							"fnComplete": function() {
								this.fnInfo( '<h3 class="no-margin-top smaller">Table copied</h3>\
									<p>Copied '+(oTable1.fnSettings().fnRecordsTotal())+' row(s) to the clipboard.</p>',
									1500
								);
							}
						},
						
						{
							"sExtends": "csv",
							"sToolTip": "Export to CSV",
							"sButtonClass": "btn btn-white btn-primary  btn-bold",
							"sButtonText": "<i class='fa fa-file-excel-o bigger-110 green'></i>"
						},
						
						{
							"sExtends": "pdf",
							"sToolTip": "Export to PDF",
							"sButtonClass": "btn btn-white btn-primary  btn-bold",
							"sButtonText": "<i class='fa fa-file-pdf-o bigger-110 red'></i>"
						},
						
						{
							"sExtends": "print",
							"sToolTip": "Print view",
							"sButtonClass": "btn btn-white btn-primary  btn-bold",
							"sButtonText": "<i class='fa fa-print bigger-110 grey'></i>",
							
							"sMessage": "<div class='navbar navbar-default'><div class='navbar-header pull-left'><a class='navbar-brand' href='#'><small>Optional Navbar &amp; Text</small></a></div></div>",
							
							"sInfo": "<h3 class='no-margin-top'>Print view</h3>\
									  <p>Please use your browser's print function to\
									  print this table.\
									  <br />Press <b>escape</b> when finished.</p>",
						}
			        ]
			    } );
				
				
				
				
				
				
			
				
			

			
			})
		 --></script>