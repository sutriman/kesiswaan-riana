
<div class="container">
<div class="row">
 <div class="login-panel panel panel-success">

<div class="panel-heading">

<h3 class="panel-title">Import Excel to MySQL</h3>
 </div>
<div class="panel-body">
<form method="post" action="import.php" enctype="multipart/form-data">
<fieldset>
<div class="form-group">
                              <input type="file" name="file"/>

 
                            </div>
                        <input class="btn btn-success" type="submit" name="submit_file" value="Submit"/>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
 

<p>
		<a href="<?php echo base_url();?>index.php/siswa/tambah" class=" btn btn-primary btn-small">Tambah Data Siswa</a>
	</p>
<table id="dynamic-table" class="table table-striped table-bordered table-hover">
		<thead>
	
				<tr>
				<td>No</td>
				<td>NISN</td>
				<td>Nama Siswa</td>
				<td>Nama Kelas</td>
				<td>Aksi</td>
			</tr>
			</thead>
			<tbody>
			
			<?php 
			$no = 1;
			foreach ($data as $row ) { 
		
				
			
			
				// $kelas = $this->model_siswa->tampil_datasiswa($row->id_kelas);
			?>
			<tr>
				<td><?php echo $no++; ?></td>
				<td><?php echo $row->NISN; ?></td>
				<td><?php echo $row->nama_siswa; ?></td>
				<td><?php echo $row->nama_kelas; ?></td>		
				
<td>
				&nbsp;&nbsp;&nbsp;&nbsp;							
					
				<a class="green" href="<?php echo base_url()?>index.php/siswa/edit/<?php echo $row->NISN; ?>">
				<i class="ace-icon fa fa-pencil bigger-130"></i>
				</a>
				
					&nbsp;&nbsp;&nbsp;&nbsp;

				<a class="red" href="<?php echo base_url()?>index.php/siswa/delete/<?php echo $row->NISN; ?>" onclick="return confirm('anda yakin akan menghapus data ini?'); ">
				<i class="ace-icon fa fa-trash-o bigger-130"></i>
				</a>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<a class="blue"  href="#modal-table" role="button" class="green" data-toggle="modal">
				<i class="ace-icon fa fa-search-plus bigger-120"></i>
				
				</a>
				</td>
			</tr>

												
				<!-- modal detail -->
<div id="modal-table" class="modal fade" tabindex="-2">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header no-padding">
												<div class="table-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														<span class="white">&times;</span>
													</button>
													Data Siswa Lengkap
												</div>
											</div>

											<div class="modal-body no-padding">
											<div class="profile-user-info profile-user-info-striped">
												<div class="profile-info-row">
													<div class="profile-info-name"> NISN </div>

													<div class="profile-info-value">
														<span class="editable" id="username"><?php echo $row->NISN ?></span>
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> Nama Siswa </div>

													<div class="profile-info-value">
														<span class="editable" id="country"><?php echo $row->nama_siswa ?></span>
														
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> Jenis Kelamin </div>

													<div class="profile-info-value">
														<span class="editable" id="age"><?php 
														if ($row->j_kelamin=='L'){ 
															echo 'Laki-laki';
															} else {
																echo 'Perempuan';
																}?></span>
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> Tempat Lahir </div>

													<div class="profile-info-value">
														<span class="editable" id="signup"><?php echo $row->tmpt_lhr ?></span>
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> Tanggal Lahir </div>

													<div class="profile-info-value">
														<span class="editable" id="login"><?php echo $row->tgl_lhr ?></span>
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> Agama </div>

													<div class="profile-info-value">
														<span class="editable" id="about"><?php echo $row->nama_agama ?></span>
												</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> Asal Sekolah </div>

													<div class="profile-info-value">
														<span class="editable" id="country"><?php echo $row->asal_sekolah ?></span>
														
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> Alamat Sekarang </div>

													<div class="profile-info-value">
														<span class="editable" id="age"><?php echo $row->alamat_sekarang ?></span>
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> Kabupaten </div>

													<div class="profile-info-value">
														<span class="editable" id="id_kabupaten"><?php echo $row->id_kabupaten ?></span>
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> Kecamatan </div>

													<div class="profile-info-value">
														<span class="editable" id="id_kecamatan"><?php echo $row->id_kecamatan ?></span>
													</div>
												</div>


												<div class="profile-info-row">
													<div class="profile-info-name"> Nama Orang Tua </div>

													<div class="profile-info-value">
														<span class="editable" id="login"><?php echo $row->nama_ortu ?></span>
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> No Telepon </div>

													<div class="profile-info-value">
														<span class="editable" id="about"><?php echo $row->no_telp ?></span>
													</div>
												</div>
												<div class="profile-info-row">
													<div class="profile-info-name"> Alamat OrangTua</div>

													<div class="profile-info-value">
														<span class="editable" id="age"><?php echo $row->alamat ?></span>
													</div>
												</div>


												

											<div>

											<div>
		
</div>
</div>
</div>


											
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div>
				<!-- end modal detail -->
					

			</td>

			</tr>
			<?php } ?>
	</tbody>
	</div>
</table>


								
						
		<script type="text/javascript">
			jQuery(function($) {
				
				var oTable1 = $('#dynamic-table').dataTable();
				
			
			
			
				//initiate TableTools extension
				var tableTools_obj = new $.fn.dataTable.TableTools( oTable1, {
					"sSwfPath": "../assets/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf", //in Ace demo ../assets will be replaced by correct assets path
					
					"sRowSelector": "td:not(:last-child)",
					"sRowSelect": "multi",
					"fnRowSelected": function($) {
						//check checkbox when row is selected
						try { $(row).find('input[type=checkbox]').get(0).checked = true }
						catch(e) {}
					},
					"fnRowDeselected": function($) {
						//uncheck checkbox
						try { $(row).find('input[type=checkbox]').get(0).checked = false }
						catch(e) {}
					},
			
					"sSelectedClass": "success",
			        "aButtons": [
						{
							"sExtends": "copy",
							"sToolTip": "Copy to clipboard",
							"sButtonClass": "btn btn-white btn-primary btn-bold",
							"sButtonText": "<i class='fa fa-copy bigger-110 pink'></i>",
							"fnComplete": function() {
								this.fnInfo( '<h3 class="no-margin-top smaller">Table copied</h3>\
									<p>Copied '+(oTable1.fnSettings().fnRecordsTotal())+' row(s) to the clipboard.</p>',
									1500
								);
							}
						},
						
						{
							"sExtends": "csv",
							"sToolTip": "Export to CSV",
							"sButtonClass": "btn btn-white btn-primary  btn-bold",
							"sButtonText": "<i class='fa fa-file-excel-o bigger-110 green'></i>"
						},
						
						{
							"sExtends": "pdf",
							"sToolTip": "Export to PDF",
							"sButtonClass": "btn btn-white btn-primary  btn-bold",
							"sButtonText": "<i class='fa fa-file-pdf-o bigger-110 red'></i>"
						},
						
						{
							"sExtends": "print",
							"sToolTip": "Print view",
							"sButtonClass": "btn btn-white btn-primary  btn-bold",
							"sButtonText": "<i class='fa fa-print bigger-110 grey'></i>",
							
							"sMessage": "<div class='navbar navbar-default'><div class='navbar-header pull-left'><a class='navbar-brand' href='#'><small>Optional Navbar &amp; Text</small></a></div></div>",
							
							"sInfo": "<h3 class='no-margin-top'>Print view</h3>\
									  <p>Please use your browser's print function to\
									  print this table.\
									  <br />Press <b>escape</b> when finished.</p>",
						}
			        ]
			    } );
				
				
				
				
				
				
			
				
			

			
			})
		 --></script>