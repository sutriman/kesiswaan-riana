

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								

								<div class="hr hr-18 hr-double dotted"></div>

								<div class="widget-box">
									<div class="widget-header widget-header-blue widget-header-flat">
										<h4 class="widget-title lighter">Form Tambah Data Siswa dan OrangTua</h4>

										
									</div>

									<div class="widget-body">
										<div class="widget-main">
											<!-- #section:plugins/fuelux.wizard -->
											<div id="fuelux-wizard-container">
												<div>
													<!-- #section:plugins/fuelux.wizard.steps -->
													<ul class="steps">
														<li data-step="1" class="active">
															<span class="step">1</span>
															<span class="title">Data Siswa</span>
														</li>

														<li data-step="2">
															<span class="step">2</span>
															<span class="title">Data Orang Tua</span>
														</li>
														<li data-step="3">
															<span class="step">3</span>
															<span class="title">Pembagian Kelas & Tahun Ajaran</span>
														</li>
													</ul>

													<!-- /section:plugins/fuelux.wizard.steps -->
												</div>

												<hr />

												<!-- #section:plugins/fuelux.wizard.container -->
												<?php 
													if ($edit == true) {
														?>
														<form class="form-horizontal" method="POST" action="<?php echo base_url();?>index.php/siswa/simpan/edit">
														<?php

													} else {	
														?>

														<form class="form-horizontal" method="POST" action="<?php echo base_url();?>index.php/siswa/simpan">

														<?php

													}
 												?>
												
												<div class="step-content pos-rel">
													<div class="step-pane active" data-step="1">
														<h3 class="lighter block green">Masukkan Data Siswa</h3>

														
															<div class="form-group">
																<label for="inputWarning" class="col-xs-12 col-sm-3 control-label no-padding-right">NISN</label>

																<div class="col-xs-12 col-sm-5">
																	<span class="block input-icon input-icon-right">
																		<input type="text" value="<?php echo $NISN; ?>" name="NISN" id="NISN" class="width-100" placeholder="Masukkan NISN" required="required" > 
																		
																	</span>
																</div>
																
															</div>
															<div class="form-group">
																<label for="inputError" class="col-xs-12 col-sm-3 col-md-3 control-label no-padding-right">Nama Siswa</label>

																<div class="col-xs-12 col-sm-5">
																	<span class="block input-icon input-icon-right">
																	<input type="text" value="<?php echo $nama_siswa; ?>" name="nama_siswa" id="nama_siswa" class="width-100" placeholder="Masukkan Nama Siswa" required="required"">	
																	</span>
																</div>
																
															</div>

															<div class="form-group">
																<label for="inputSuccess" class="col-xs-12 col-sm-3 control-label no-padding-right">Jenis Kelamin</label>

																<div class="col-xs-12 col-sm-5">
																	<select id="j_kelamin" name="j_kelamin" class="width-100" data-placeholder="-- Pilih --">
																	<option value="">-- Pilih --</option>
																	<option value="L">Laki-Laki</option>
																	<option value="P">Perempuan</option>
																	</select>
																	
																	 
																</div>
																
															</div>

															<div class="form-group">
																<label for="inputInfo" class="col-xs-12 col-sm-3 control-label no-padding-right">Tempat Lahir</label>

																<div class="col-xs-12 col-sm-5">
																	<span class="block input-icon input-icon-right">
																		<input type="text" value="<?php echo $tmpt_lhr; ?>" name="tmpt_lhr" id="tmpt_lhr" class="width-100" placeholder="Masukkan Tempat Lahir " required="required">
																		
																	</span>
																</div>
																
															</div>
															<div class="form-group">
																<label for="inputError2" class="col-xs-12 col-sm-3 control-label no-padding-right">Tanggal Lahir</label>

																<div class="col-xs-12 col-sm-5">
																	<span class="input-icon block">
																		<input type="date" value="<?php echo $tgl_lhr; ?>" name="tgl_lhr" id="tgl_lhr" class="width-100" placeholder="Masukkan " required="required"> 
																		
																	</span>
																	
																</div>
																
															</div>

															<div class="form-group">
																<label for="inputError2" class="col-xs-12 col-sm-3 control-label no-padding-right">Agama</label>

																<div class="col-xs-12 col-sm-5">


																<select name="id_agama" id="id_agama" class="width-100" data-placeholder="--Pilih--">
																		<option value="">-- Pilih --</option>
																		<?php foreach ($agamaa as $ag) { ?>
																		<option value="<?php echo $ag->id_agama; ?>"><?php echo $ag->nama_agama; ?></option>
																		<?php } ?>
																	</select>
																	
																	 
																</div>
																
															</div>
															
															

															<div class="form-group">
																<label for="inputError2" class="col-xs-12 col-sm-3 control-label no-padding-right">Asal Sekolah</label>

																<div class="col-xs-12 col-sm-5">
																	<span class="input-icon block">
																	<input type="text" value="<?php echo $asal_sekolah; ?>" name="asal_sekolah" id="asal_sekolah" class="width-100" placeholder="Masukkan Asal Sekolah" required="required"> 	
																		
																	</span>
																	 
																</div>
																
															</div>
															

															<div class="form-group">
																<label for="inputError2" class="col-xs-12 col-sm-3 control-label no-padding-right">Alamat Sekarang</label>

																<div class="col-xs-12 col-sm-5">
																	<span class="input-icon block">
																	<input type="text" value="<?php echo $alamat_sekarang; ?>" name="alamat_sekarang" id="alamat_sekarang" class="width-100" placeholder="Masukkan Alamat Sekarang " required="required">	
																	
																	</span>
																	
																</div>
																
															</div>



															<div class="form-group">
																<label for="inputError2" class="col-xs-12 col-sm-3 control-label no-padding-right">Kabupaten</label>

																<div class="col-xs-12 col-sm-5">
																	
																	<select name="id_kabupaten" id="id_kabupaten" class="width-100" data-placeholder="--Pilih--">
																		<option value="">-- Pilih --</option>
																		<?php foreach ($kabupaten as $kb) { ?>
																		<option value="<?php echo $kb->id_kabupaten; ?>"><?php echo $kb->nama_kabupaten; ?></option>
																		<?php } ?>
																	</select>
																</div>																																											
															</div>

															<div class="form-group">
																<label for="inputError2" class="col-xs-12 col-sm-3 control-label no-padding-right">Kecamatan</label>

																<div class="col-xs-12 col-sm-5">
																	
																	<select name="id_kecamatan" id="id_kecamatan" class="width-100" data-placeholder="--Pilih--">
																		<option value="">-- Pilih --</option>
																		<?php foreach ($kecamatan as $kcm) { ?>
																		<option value="<?php echo $kcm->id_kecamatan; ?>"><?php echo $kcm->nama_kecamatan; ?></option>
																		<?php } ?>
																	</select>
																</div>																																											
															</div>


</form>
</div>															

													<div class="step-pane" data-step="2">
														<h3 class="lighter block green">Masukkan Data Orang Tua</h3>
														
															<div class="form-group">
																<label for="inputError2" class="col-xs-12 col-sm-3 control-label no-padding-right">Nama Orang Tua</label>

																<div class="col-xs-12 col-sm-5">
																	<span class="input-icon block">
																	<input type="text" value="<?php echo $nama_ortu; ?>" name="nama_ortu" id="nama_ortu" class="width-100" placeholder="Masukkan Nama OrangTua" required="required">
																	
																	</span>
																	
																</div>
																
															</div>

														<div class="form-group">
																<label for="inputError2" class="col-xs-12 col-sm-3 control-label no-padding-right">Alamat OrangTua</label>

																<div class="col-xs-12 col-sm-5">
																	<span class="input-icon block">
																	<input type="text" value="<?php echo $alamat; ?>" name="alamat" id="alamat" class="width-100" placeholder="Masukkan Alamat OrangTua " required="required"> 
																	
																	</span>
																	
																</div>
																
															</div>

															<div class="form-group">
																<label for="inputError2" class="col-xs-12 col-sm-3 control-label no-padding-right">No Telepon</label>

																<div class="col-xs-12 col-sm-5">
																	<span class="input-icon block">
																	<input type="text" value="<?php echo $no_telp; ?>" name="no_telp" id="no_telp" class="form-control" placeholder="Masukkan No Telepon " required="required"> 
																	
																	</span>
																	
																</div>
																
															</div>

															
														</form>
													</div>

													<div class="step-pane" data-step="3">
														<h3 class="lighter block green">Pembagian Kelas & Tahun Ajaran</h3>

														<div class="form-group">
																<label for="inputError2" class="col-xs-12 col-sm-3 control-label no-padding-right">Kode Kelas</label>

																<div class="col-xs-12 col-sm-5">
																	
																	<select name="id_kelas" id="id_kelas" class="width-100" data-placeholder="--Pilih--">
																		<option value="">-- Pilih --</option>
																		<?php foreach ($kelass as $kls) { ?>
																		<option value="<?php echo $kls->id_kelas; ?>"><?php echo $kls->nama_kelas; ?></option>
																		<?php } ?>
																	</select>
																</div>																																											
															</div>

															<div class="form-group">
																<label for="inputError2" class="col-xs-12 col-sm-3 control-label no-padding-right">Tahun Ajaran</label>

																<div class="col-xs-12 col-sm-5">

																	<select name="id_tahun_ajaran" class="width-100" id="id_tahun_ajarann" data-placeholder="--Pilih--">
																		<option value="">-- Pilih --</option>
																		<?php foreach ($tahun_ajaran as $thn) { ?>
																		<option value="<?php echo $thn->id_tahun_ajaran; ?>"><?php echo $thn->tahun_ajaran; ?></option>
																		<?php } ?>
																	</select>
																	
																</div>
																
															</div>
															
															<button type="submit" class="btn btn-primary btn-small">Simpan</button>
													</div>
													

												</form>
												</div>
									

											<hr />

											<!-- /section:plugins/fuelux.wizard -->
										</div><!-- /.widget-main -->
											<div class="wizard-actions">
												<!-- #section:plugins/fuelux.wizard.buttons -->
												<button class="btn btn-prev">
													<i class="ace-icon fa fa-arrow-left"></i>
													Kembali
												</button>

												<button class="btn btn-success btn-next">
													Lanjut
													<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
												</button>
											</div>
									</div><!-- /.widget-body -->
								</div>

			
		

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='<?php echo base_url();?>assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='<?php echo base_url();?>assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url();?>assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->
		<script src="<?php echo base_url();?>assets/js/fuelux/fuelux.wizard.js"></script>
		<script src="<?php echo base_url();?>assets/js/jquery.validate.js"></script>
		<script src="<?php echo base_url();?>assets/js/additional-methods.js"></script>
		<script src="<?php echo base_url();?>assets/js/bootbox.js"></script>
		<script src="<?php echo base_url();?>assets/js/jquery.maskedinput.js"></script>
		

		
		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
$('#id_tahun_ajarann').select2()
			
				$('[data-rel=tooltip]').tooltip();
			
				// $(".select2").css('width','200px').select2({allowClear:true})
				// .on('change', function(){
				// 	$(this).closest('form').validate().element($(this));
				// }); 
			
			
				var $validation = false;
				$('#fuelux-wizard-container')
				.ace_wizard({
					//step: 2 //optional argument. wizard will jump to step "2" at first
					//buttons: '.wizard-actions:eq(0)'
				})
				.on('actionclicked.fu.wizard' , function(e, info){
					if(info.step == 1 && $validation) {
						if(!$('#validation-form').valid()) e.preventDefault();
					}
				})
				.on('simpan', function(e) {
					bootbox.dialog({
						message: "Data telah disimpan!", 
						buttons: {
							"success" : {
								"label" : "OK",
								"className" : "btn-sm btn-primary"
							}
						}
					});
				}).on('stepclick.fu.wizard', function(e){
					//e.preventDefault();//this will prevent clicking and selecting steps
				});
			
			
				//jump to a step
				/**
				var wizard = $('#fuelux-wizard-container').data('fu.wizard')
				wizard.currentStep = 3;
				wizard.setState();
				*/
			
				//determine selected step
				//wizard.selectedItem().step
			
			
			
				//hide or show the other form which requires validation
				//this is for demo only, you usullay want just one form in your application
				$('#skip-validation').removeAttr('checked').on('click', function(){
					$validation = this.checked;
					if(this.checked) {
						$('#sample-form').hide();
						$('#validation-form').removeClass('hide');
					}
					else {
						$('#validation-form').addClass('hide');
						$('#sample-form').show();
					}
				})
			
			
			
				//documentation : http://docs.jquery.com/Plugins/Validation/validate
			
			
				$.mask.definitions['~']='[+-]';
				$('#phone').mask('(999) 999-9999');
			
				jQuery.validator.addMethod("phone", function (value, element) {
					return this.optional(element) || /^\(\d{3}\) \d{3}\-\d{4}( x\d{1,6})?$/.test(value);
				}, "Enter a valid phone number.");
			
				$('#validation-form').validate({
					errorElement: 'div',
					errorClass: 'help-block',
					focusInvalid: false,
					ignore: "",
					rules: {
						email: {
							required: true,
							email:true
						},
						password: {
							required: true,
							minlength: 5
						},
						password2: {
							required: true,
							minlength: 5,
							equalTo: "#password"
						},
						name: {
							required: true
						},
						phone: {
							required: true,
							phone: 'required'
						},
						url: {
							required: true,
							url: true
						},
						comment: {
							required: true
						},
						state: {
							required: true
						},
						platform: {
							required: true
						},
						subscription: {
							required: true
						},
						gender: {
							required: true,
						},
						agree: {
							required: true,
						}
					},
			
					messages: {
						email: {
							required: "Please provide a valid email.",
							email: "Please provide a valid email."
						},
						password: {
							required: "Please specify a password.",
							minlength: "Please specify a secure password."
						},
						state: "Please choose state",
						subscription: "Please choose at least one option",
						gender: "Please choose gender",
						agree: "Please accept our policy"
					},
			
			
					highlight: function (e) {
						$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
					},
			
					success: function (e) {
						$(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
						$(e).remove();
					},
			
					errorPlacement: function (error, element) {
						if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
							var controls = element.closest('div[class*="col-"]');
							if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
							else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
						}
						else if(element.is('.select2')) {
							error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
						}
						else if(element.is('.chosen-select')) {
							error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
						}
						else error.insertAfter(element.parent());
					},
			
					submitHandler: function (form) {
					},
					invalidHandler: function (form) {
					}
				});
			
				
				
				
				$('#modal-wizard-container').ace_wizard();
				$('#modal-wizard .wizard-actions .btn[data-dismiss=modal]').removeAttr('disabled');
				
				
				/**
				$('#date').datepicker({autoclose:true}).on('changeDate', function(ev) {
					$(this).closest('form').validate().element($(this));
				});
				
				$('#mychosen').chosen().on('change', function(ev) {
					$(this).closest('form').validate().element($(this));
				});
				*/
				
				
				$(document).one('ajaxloadstart.page', function(e) {
					//in ajax mode, remove remaining elements before leaving page
					$('[class*=select2]').remove();
				});
			})
		</script>

		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ace.onpage-help.css" />
		<!-- <link rel="stylesheet" href="<?php echo base_url();?>docs/assets/js/themes/sunburst.css" /> -->

		<script type="text/javascript"> ace.vars['base'] = '..'; </script>
		<script src="<?php echo base_url();?>assets/js/ace/elements.onpage-help.js"></script>
		<script src="<?php echo base_url();?>assets/js/ace/ace.onpage-help.js"></script>
		<!-- <script src="<?php echo base_url();?>docs/assets/js/rainbow.js"></script>
		<script src="<?php echo base_url();?>docs/assets/js/language/generic.js"></script>
		<script src="<?php echo base_url();?>docs/assets/js/language/html.js"></script>
		<script src="<?php echo base_url();?>docs/assets/js/language/css.js"></script>
		<script src="<?php echo base_url();?>docs/assets/js/language/javascript.js"></script> -->
	</body>
</html>