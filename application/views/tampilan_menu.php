				<ul class="nav nav-list">
					<li class="">
						<a href="<?php echo site_url('home'); ?>">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Dashboard </span>
						</a>

						<b class="arrow"></b>
					</li>

					<li class="">
						<a href="<?php echo base_url();?>index.php/agama">
							<i class="menu-icon fa fa-desktop"></i>
							<span class="menu-text">
								Data Agama
							</span>
						</a>

						<b class="arrow"></b>
						</li>

					<li class="">
						<a href="<?php echo base_url();?>index.php/kelas">
							<i class="menu-icon fa fa-desktop"></i>
							<span class="menu-text">
								Data Kelas
							</span>
						</a>

						<b class="arrow"></b>
						</li>


						<li class="">
						<a href="<?php echo base_url();?>index.php/siswa">
							<i class="menu-icon fa fa-desktop"></i>
							<span class="menu-text">
								Data Siswa
							</span>
						</a>

						<b class="arrow"></b>
						</li>

						<li class="">
						<a href="<?php echo base_url();?>index.php/tahunajaran">
							<i class="menu-icon fa fa-desktop"></i>
							<span class="menu-text">
								Data Tahun Ajaran
							</span>
						</a>

						<b class="arrow"></b>
						</li>

						

						<li class="">
						<a href="<?php echo base_url();?>index.php/siswakelas">
							<i class="menu-icon fa fa-desktop"></i>
							<span class="menu-text">
								Kenaikan Kelas
							</span>
						</a>

						<b class="arrow"></b>
						</li>

						<li class="">
					<a href="<?php echo site_url('home'); ?>"" class="dropdown-toggle">
							<i class="menu-icon fa fa-print"></i>
							<span class="menu-text"> Laporan </span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>


						<ul class="submenu">
						<li class="">
						<a href="<?php echo base_url();?>index.php/laporan/laporan1">
							<i class="menu-icon fa fa-print"></i>
							<span class="menu-text"> Jumlah Siswa Berdasarkan Jenis Kelamin </span>
						</a>

						<b class="arrow"></b>
						</li>

							<li class="">
								<a href="<?php echo base_url();?>index.php/laporan/laporan2">
									<i class="menu-icon fa fa-print"></i>
									<span class="menu-text"> Jumlah Siswa Berdasarkan Tempat Tinggal </span>
								</a>

								<b class="arrow"></b>
							</li>
							<li class="">
								<a href="<?php echo base_url();?>index.php/laporan/laporan3">
									<i class="menu-icon fa fa-print"></i>
									<span class="menu-text"> Jumlah Siswa Berdasarkan Agama </span>
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="<?php echo base_url();?>index.php/laporan/laporan4">
									<i class="menu-icon fa fa-print"></i>
									<span class="menu-text">Jumlah Siswa Berdasarkan Tahun Kelahiran</span>
								</a>

								<b class="arrow"></b>
							</li>
							
							<li class="">
								<a href="<?php echo base_url();?>index.php/laporan/laporan5">
									<i class="menu-icon fa fa-print"></i>
									<span class="menu-text">Jumlah Siswa Berdasarkan Umur</span>
								</a>

								<b class="arrow"></b>
							</li>
							
							
						</ul>
							</li>

							<li class="">
						<a href="<?php echo base_url();?>index.php/admins">
							<i class="menu-icon  fa fa-users"></i>
							<span class="menu-text">
								User
							</span>
						</a>

						<b class="arrow"></b>
						</li>


							<li class="">
						<a href="<?php echo base_url();?>index.php/login">
							<i class="menu-icon fa fa-power-off"></i>
							<span class="menu-text"> Keluar </span>

						</a>

						<b class="arrow"></b>
						</li>
						</ul>
						</ul>
						