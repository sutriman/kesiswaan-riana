<div class="space-6"></div>

										<div class="widget-box">
											<div class="widget-header widget-header-small">
												<h5 class="widget-title lighter">Form Pencarian</h5>
											</div>

											<div class="widget-body">
												<div class="widget-main">
													<form class="form-search" action="<?php echo site_url('siswakelas/index/cari');  ?>" method="post">
														<div class="row">

															

                                             <div class="col-sm-3">
                                                <div class="input-group">
                                                   <div class="col-sm-6">
                                                      <label>Tahun Ajaran</label>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <!-- <input type="hidden" value="1" name="tahun_id"> -->
                                                      <select name="tahun" id="id_kelas" data-placeholder="--Pilih--">
                                                      <option value="">-- Pilih --</option>
                                                      <?php foreach ($tahun_ajaran as $a) { ?>
                                                      <option value="<?php echo $a->id_tahun_ajaran; ?>"><?php echo $a->tahun_ajaran; ?></option>
                                                      <?php } ?>
                                                   </select>
                                                      <!-- <input type="text"  name="tahun" class="form-control search-query" placeholder="Cari Data" /> -->
                                                   
                                                   </div>
                                                </div>
                                                   
                                             </div>


															<div class="col-sm-4">
																<div class="input-group">
																	<div class="col-sm-3">
																		<label>Kelas</label>
																	</div>

																	<div class="col-sm-7">


																	<select name="id_kelas" id="id_kelas" class="width-100" data-placeholder="--Pilih--">
																		<option value="">-- Pilih --</option>
																		<?php foreach ($kelas as $kls) { ?>
																		<option value="<?php echo $kls->id_kelas; ?>"><?php echo $kls->nama_kelas; ?></option>
																		<?php } ?>
																	</select>
																		<!-- <select name="kelas">
																			<option value="101">X IPA 1</option>
																		</select> -->

																	</div>
																	<div class="col-sm-2">
																		<span class="input-group-btn">
																		<button type="submit" class="btn btn-purple btn-sm">
																			<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
																			Cari
																		</button>
																		</span>
																	</div>
																	
															</div>

														</div>

														</form>

														<form action="<?php echo site_url('siswakelas/pindah_t_ajaran');  ?>" method="POST" enctype="multipart/form-data">
														<input type="hidden" value="<?php echo $tahun_l; ?>" name="tahun_baru">
														<input type="hidden" value="<?php echo $kelas_l; ?>" name="kelas_l">

														<div class="col-sm-3">
																<div class="input-group">
																	<div class="col-sm-6">
																		<label>Naik ke Kelas</label>
																	</div>

																	<!-- <div class="col-sm-6"> -->

																	<div class="col-sm-6">
																		<select name="id_kelas" id="id_kelas" class="width-100" data-placeholder="--Pilih--">
																		<option value="">-- Pilih --</option>
																		<?php foreach ($kelas as $kls) { ?>
																		<option value="<?php echo $kls->id_kelas; ?>"><?php echo $kls->nama_kelas; ?></option>
																		<?php } ?>
																	</select>
																	</div>

																	<div class="col-sm-9">
																		<span class="input-group-btn">
																		<button type="submit" class="btn btn-purple btn-sm">
																			<span class="ace-icon-on-right bigger-110"></span>
																			Pindah
																		</button>
																		</span>
																	</div>
														</div></div>
																	
										</div>
												</div>
											</div>
</div>



										<div class="table-header">
											Menampilkan Table Hasil Pencarian
										</div>
											


										<div>
											<table id="dynamic-table" class="table table-striped table-bordered table-hover">
												<thead>
													<tr>

				<td>Pilih</td>
				<td>NISN</td>
				<td>Nama Kelas</td>
				<td>Tahun Ajaran</td>
				<td>Keterangan</td>
			</tr>
			</thead>
			<tbody>
			
			<?php 

			if ($hasil_cari == null) {

			} else {
			$no = 1;
			foreach ($hasil_cari as $row ) { 
			
			?>
			<tr>
				<td><input type="checkbox" name="id_siswa[]" value="<?php echo $row->NISN;?>"></td>
				<td><?php echo $row->NISN; ?></td>
				<td><?php echo $row->nama_kelas; ?></td>
				<td><?php echo $row->tahun_ajaran; ?></td>	
				<td><?php echo $row->status_lulus; ?></td>	
			</tr>
													
					
			<?php } } ?>
	</tbody>
	</div>

</table>

</div>


</form>

										<script type="text/javascript">
			jQuery(function($) {
				
				var oTable1 = $('#dynamic-table').dataTable( {
					bAutoWidth: false,
					"aoColumns": [
					  { "bSortable": false },
					  null, null,null, null, null, null,
					  { "bSortable": false }
					],
					"aaSorting": [],
			
					//,
					//"sScrollY": "200px",
					//"bPaginate": false,
			
					//"sScrollX": "100%",
					//"sScrollXInner": "120%",
					//"bScrollCollapse": true,
					//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
					//you may want to wrap the table inside a "div.dataTables_borderWrap" element
			
					//"iDisplayLength": 50
			    } );
				// //TableTools settings
				// TableTools.classes.container = "btn-group btn-overlap";
				// TableTools.classes.print = {
				// 	"body": "DTTT_Print",
				// 	"info": "tableTools-alert gritter-item-wrapper gritter-info gritter-center white",
				// 	"message": "tableTools-print-navbar"
				// }
			
				//initiate TableTools extension
				var tableTools_obj = new $.fn.dataTable.TableTools( oTable1, {
					// "sSwfPath": "../assets/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf", //in Ace demo ../assets will be replaced by correct assets path
					
					// "sRowSelector": "td:not(:last-child)",
					// "sRowSelect": "multi",
					// "fnRowSelected": function(row) {
					// 	//check checkbox when row is selected
					// 	try { $(row).find('input[type=checkbox]').get(0).checked = true }
					// 	catch(e) {}
					// },
					// "fnRowDeselected": function(row) {
					// 	//uncheck checkbox
					// 	try { $(row).find('input[type=checkbox]').get(0).checked = false }
					// 	catch(e) {}
					// },
			
					"sSelectedClass": "success",
			        "aButtons": [
						{
							"sExtends": "copy",
							"sToolTip": "Copy to clipboard",
							"sButtonClass": "btn btn-white btn-primary btn-bold",
							"sButtonText": "<i class='fa fa-copy bigger-110 pink'></i>",
							"fnComplete": function() {
								this.fnInfo( '<h3 class="no-margin-top smaller">Table copied</h3>\
									<p>Copied '+(oTable1.fnSettings().fnRecordsTotal())+' row(s) to the clipboard.</p>',
									1500
								);
							}
						},
						
							);
							}
						},
						
		
			        ]
			    } );
			
			
			})
		</script>		