/*
SQLyog Ultimate v10.42 
MySQL - 5.5.5-10.1.16-MariaDB : Database - sekolah
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sekolah` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sekolah`;

/*Table structure for table `admins` */

DROP TABLE IF EXISTS `admins`;

CREATE TABLE `admins` (
  `id_username` char(10) NOT NULL,
  `username` char(10) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id_username`,`username`,`password`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `admins` */

insert  into `admins`(`id_username`,`username`,`password`) values ('1','admin','21232f297a57a5a743894a0e4a801fc3'),('2','admin2','c84258e9c39059a89ab77d846ddab909');

/*Table structure for table `agama` */

DROP TABLE IF EXISTS `agama`;

CREATE TABLE `agama` (
  `id_agama` int(2) NOT NULL,
  `nama_agama` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_agama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `agama` */

insert  into `agama`(`id_agama`,`nama_agama`) values (1,'Islam'),(2,'Kristen'),(3,'Katholik'),(4,'Hindu'),(5,'Budha');

/*Table structure for table `kabupaten` */

DROP TABLE IF EXISTS `kabupaten`;

CREATE TABLE `kabupaten` (
  `id_kabupaten` char(10) NOT NULL,
  `nama_kabupaten` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_kabupaten`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kabupaten` */

insert  into `kabupaten`(`id_kabupaten`,`nama_kabupaten`) values ('101','Kab.Bantul'),('102','Kab.Gunungkidul'),('103','Kab.Kulon Progo'),('104','Kab.Sleman'),('105','Kota Yogyakarta');

/*Table structure for table `kecamatan` */

DROP TABLE IF EXISTS `kecamatan`;

CREATE TABLE `kecamatan` (
  `id_kecamatan` char(10) NOT NULL,
  `nama_kecamatan` varchar(20) DEFAULT NULL,
  `id_kabupaten` char(10) DEFAULT NULL,
  PRIMARY KEY (`id_kecamatan`),
  KEY `id_kabupaten` (`id_kabupaten`),
  CONSTRAINT `kecamatan_ibfk_1` FOREIGN KEY (`id_kabupaten`) REFERENCES `kabupaten` (`id_kabupaten`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kecamatan` */

insert  into `kecamatan`(`id_kecamatan`,`nama_kecamatan`,`id_kabupaten`) values ('110','Srandakan','101'),('111','Sewon','101'),('112','Sedayu','101'),('113','Sanden','101'),('114','Pundong	','101'),('115','Pleret','101'),('116','Piyungan','101'),('117','Pandak','101'),('118','Pajangan','101'),('119','Kretek','101'),('120','Kasihan','101'),('121','Jetis','101'),('122','Imogiri','101'),('123','Dlingo','101'),('124','Bantul','101'),('125','Banguntapan','101'),('126','Bambanglipiro','101'),('221','Gedangsari','102'),('222','Girisubo','102'),('223','Karangmojo','102'),('224','Ngawen','102'),('225','Nglipar','102'),('226','Paliyan','102'),('227','Panggang','102'),('228','Patuk','102'),('229','Playan','102'),('230','Ponjong','102'),('231','Purwosari','102'),('232','Rongkop','102'),('234','Saptosari','102'),('235','Semanu','102'),('236','Semin','102'),('237','Tanjungsari','102'),('238','Tepus','102'),('239','Wonosari','102'),('310','Galur','103'),('311','Girimulyo','103'),('312','Kalibawang','103'),('313','Kokap','103'),('314','Lendah','103'),('315','Nanggulan','103'),('316','Panjatan','103'),('317','Pengasih','103'),('318','Samigaluh','103'),('319','Sentolo','103'),('320','Temon','103'),('321','Wates','103'),('410','Berbah','104'),('411','Cangkringan','104'),('412','Depok','104'),('413','Gamping','104'),('414','Godean','104'),('415','Kalasan','104'),('416','Minggir','104'),('417','Mlati','104'),('418','Moyudan','104'),('419','Ngaglik','104'),('420','Ngemplak','104'),('421','Pakem','104'),('422','Prambanan','104'),('423','Sayegan','104'),('424','Sleman','104'),('425','Tempel','104'),('426','Turi','104'),('510','Mantrijeron','105'),('511','Kraton','105'),('512','Mergangsan','105'),('513','Umbulharjo','105'),('514','Kotagede','105'),('515','Gondokusuma','105'),('516','Danurejan','105'),('517','Pakualaman','105'),('518','Gondomanan','105'),('519','Ngampilan','105'),('520','Wirobrajan','105'),('521','Gedongtengen','105'),('522','Jetis','105'),('523','Tegalrejo','105');

/*Table structure for table `kelas` */

DROP TABLE IF EXISTS `kelas`;

CREATE TABLE `kelas` (
  `id_kelas` int(10) NOT NULL,
  `nama_kelas` varchar(20) DEFAULT NULL,
  `status` enum('1','2') DEFAULT NULL,
  PRIMARY KEY (`id_kelas`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kelas` */

insert  into `kelas`(`id_kelas`,`nama_kelas`,`status`) values (101,'X IPA 1',NULL),(102,'X IPA 2',NULL),(103,'X IPA 3',NULL),(104,'XI IPA 1',NULL);

/*Table structure for table `siswa` */

DROP TABLE IF EXISTS `siswa`;

CREATE TABLE `siswa` (
  `NISN` char(10) NOT NULL,
  `nama_siswa` varchar(30) DEFAULT NULL,
  `j_kelamin` enum('L','P') DEFAULT NULL,
  `tmpt_lhr` varchar(225) DEFAULT NULL,
  `tgl_lhr` date DEFAULT NULL,
  `id_agama` int(2) DEFAULT NULL,
  `asal_sekolah` varchar(10) DEFAULT NULL,
  `alamat_sekarang` text,
  `nama_ortu` varchar(30) DEFAULT NULL,
  `no_telp` decimal(10,0) DEFAULT NULL,
  `alamat` text,
  `id_kecamatan` char(10) DEFAULT NULL,
  `id_kabupaten` char(10) DEFAULT NULL,
  PRIMARY KEY (`NISN`),
  KEY `id_agama` (`id_agama`),
  KEY `id_kecamatan` (`id_kecamatan`),
  KEY `id_kabupaten` (`id_kabupaten`),
  CONSTRAINT `siswa_ibfk_1` FOREIGN KEY (`id_agama`) REFERENCES `agama` (`id_agama`) ON UPDATE CASCADE,
  CONSTRAINT `siswa_ibfk_2` FOREIGN KEY (`id_kecamatan`) REFERENCES `kecamatan` (`id_kecamatan`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `siswa_ibfk_3` FOREIGN KEY (`id_kabupaten`) REFERENCES `kabupaten` (`id_kabupaten`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `siswa` */

insert  into `siswa`(`NISN`,`nama_siswa`,`j_kelamin`,`tmpt_lhr`,`tgl_lhr`,`id_agama`,`asal_sekolah`,`alamat_sekarang`,`nama_ortu`,`no_telp`,`alamat`,`id_kecamatan`,`id_kabupaten`) values ('10983','andi fir','L','yogyakarta','1995-09-11',2,'smp 1','jakarta','eko',10829070,'jgj',NULL,NULL),('456','andi nur','L','jogja','2000-01-11',2,'smp 1','jakarta','eko',10829070,'jgj',NULL,NULL),('800','ahmad d','L','yogyakarta','2000-06-11',1,'smp 1','jakarta','ira',209823,'jgj',NULL,NULL);

/*Table structure for table `siswa_kelas` */

DROP TABLE IF EXISTS `siswa_kelas`;

CREATE TABLE `siswa_kelas` (
  `NISN` char(10) NOT NULL,
  `id_kelas` int(10) NOT NULL,
  `id_tahun_ajaran` int(10) NOT NULL,
  `status_lulus` enum('naik','belum naik','') NOT NULL,
  PRIMARY KEY (`NISN`,`id_kelas`,`id_tahun_ajaran`,`status_lulus`),
  KEY `id_kelas` (`id_kelas`),
  KEY `id_tahun_ajaran` (`id_tahun_ajaran`),
  CONSTRAINT `siswa_kelas_ibfk_1` FOREIGN KEY (`NISN`) REFERENCES `siswa` (`NISN`) ON UPDATE CASCADE,
  CONSTRAINT `siswa_kelas_ibfk_2` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`) ON UPDATE CASCADE,
  CONSTRAINT `siswa_kelas_ibfk_3` FOREIGN KEY (`id_tahun_ajaran`) REFERENCES `tahun_ajaran` (`id_tahun_ajaran`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `siswa_kelas` */

insert  into `siswa_kelas`(`NISN`,`id_kelas`,`id_tahun_ajaran`,`status_lulus`) values ('10983',101,2,'naik'),('456',102,1,'naik'),('456',103,2,''),('800',101,1,'naik');

/*Table structure for table `tahun_ajaran` */

DROP TABLE IF EXISTS `tahun_ajaran`;

CREATE TABLE `tahun_ajaran` (
  `id_tahun_ajaran` int(10) NOT NULL,
  `tahun_ajaran` varchar(10) DEFAULT NULL,
  `status` enum('1','2') DEFAULT NULL,
  PRIMARY KEY (`id_tahun_ajaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tahun_ajaran` */

insert  into `tahun_ajaran`(`id_tahun_ajaran`,`tahun_ajaran`,`status`) values (1,'2014/2015','1'),(2,'2016/2017','1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
